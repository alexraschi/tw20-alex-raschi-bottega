$(document).ready(function()
{
	'use strict';
	const password = $('input[type=password]');
	$('input#show-password').change(function()
	{
		if (this.checked)
		{
			password.attr('type', 'text');
		}
		else
		{
			password.attr('type', 'password');
		}
	});
});
