$(document).ready(function()
{
	'use strict';
	const password = $('input#password');
	const check = $('input#password-check');
	$('form').submit(function(event)
	{
		if (password.val() !== check.val())
		{
			event.preventDefault();
			event.stopPropagation();
			alert('Le due password non coincidono!');
		}
	});
});
