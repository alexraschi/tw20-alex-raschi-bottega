$(document).ready(function()
{
	'use strict';
	const target = $('div.target');
	const inputs = $('div.target input');
	target.hide();
	$('input#target').change(function()
	{
		if (this.checked)
		{
			target.show();
			inputs.each(function()
			{
				$(this).prop('required', true);
			});
		}
		else
		{
			target.hide();
			inputs.each(function()
			{
				$(this).prop('required', false);
			});
		}
	});
});
