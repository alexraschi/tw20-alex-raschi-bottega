insert into category(id, name) values
	(1,	'Tisane'),
	(2,	'Biscotti e prodotti da forno'),
	(3,	'Cereali per la colazione'),
	(4,	'Cereali'),
	(5,		'Prodotti per dolci'),
	(6,		'Creme spalmabili e marmellate'),
	(7,	'Dolcificanti'),
	(8,	'Spezie'),
	(9,	'Conserve e verdure'),
	(10,	'Estratti e succhi'),
	(11,	'Frutta secca'),
	(12,		'Semi oleosi'),
	(13,		'Sale'),
	(14,		'Pasta'),
	(15,	'Lieviti'),
	(16,		'Cacao');

insert into product
	(
		name,
		ingredients,
		weight,
		cost,
		description,
		deadline,
		image,
		category,
		quantity
	)
	values
	(
		'Infuso frutti di bosco',
		'mela, rosacanina, pera, mirtillo, rooibos.',
		90,
		2.00,
		'Infuso di mela, rosacanina, pera, mirtillo e rooibos per tisana.',
		'2023-03-31',
		'infuso-frutti-di-bosco.jpg',
		1,
		43
	),
	(
		'Amaretti',
		'biscotti artigianali, mandorle.',
		60,
		5.10,
		'Biscotti artigianali fatti con materie prime di qualità tra cui mandorle italiane.',
		'2022-05-23',
		'amaretti.jpg',
		2,
		9
	),
	(
		'Muesli',
		'mais, riso, uva sultanina, frutta secca, semi oleosi.',
		400,
		4.30,
		'Muesli contenente fiocchi di mais, riso soffiato, uvetta sultanina, mandorle, nocciole, semi di girasole e zucca.',
		'2021-04-12',
		'muesli.jpg',
		3,
		11
	),
	(
		'Malva',
		'malva.',
		200,
		1.20,
		'Fiori e foglie di malva essiccate per tisana.',
		'2022-11-03',
		'malva.jpg',
		1,
		92
	),
	(
		'Avena',
		'avena.',
		450,
		3.20,
		'Fiocchi di avena per la colazione.',
		'2023-10-05',
		'avena.jpg',
		3,
		21
	),
	(
		'Cacao in polvere',
		'cacao.',
		100,
		5.70,
		'Cacao in polvere',
		'2021-03-23',
		'cacao.jpg',
		5,
		63
	),
	(
		'Nocciolata',
		'zucchero di canna, pasta di nocciole, olio di semi di girasole, latte scremato in polvere, cacao magro in polvere, burro di cacao, emulsionante: leticina di soia, estratto di vaniglia.',
		270,
		4.30,
		'Crema spalmabile al cacao e nocciole biologica.',
		'2023-07-02',
		'nocciolata.jpg',
		6,
		12
	),
	(
		'Miele di acacia',
		'miele.',
		1000,
		2.60,
		'Miele di acacia prodotto nella provincia di Rimini.',
		'2022-07-01',
		'miele-acacia.jpg',
		7,
		10
	),
	(
		'Rosmarino',
		'rosmarino.',
		50,
		1.45,
		'Rosmarino essiccato, prodotto durante l\'estate 2020.',
		'2023-04-21',
		'rosmarino.jpg',
		8,
		2
	),
	(
		'Peperoncino',
		'peperoncino.',
		65,
		4.85,
		'Peperoncino essiccato, prodotto durante l\'estate 2020.',
		'2022-07-25',
		'peperoncino.jpg',
		8,
		16
	),
	(
		'Aglio macinato',
		'aglio.',
		40,
		5.15,
		'Aglio essiccato e macinato, selezionato nelle migliori coltivazioni in tutto il mondo.',
		'2022-12-17',
		'aglio.jpg',
		8,
		55
	),
	(
		'Timo',
		'timo.',
		20,
		3.45,
		'Timo essiccato, prodotto durante l\'estate 2020.',
		'2021-06-15',
		'timo.jpg',
		8,
		5
	),
	(
		'Paprika dolce',
		'peperoni.',
		25,
		5.65,
		'Miscela di peperoni rossi aromatici essiccati e macinati, selezionati nelle migliori coltivazioni di tutto il mondo.',
		'2022-12-06',
		'paprika.jpg',
		8,
		19
	),
	(
		'Cumino indiano intero',
		'cumino.',
		35,
		3.50,
		'Le erbe di Rocca dei Fiori sono coltivate ed essiccate con la massima cura al fine di mantenere inalterate le proprietà delle piante.',
		'2021-01-31',
		'cumino.jpg',
		8,
		26
	),
	(
		'Curry',
		'curcuma, coriandolo, cumino, sale, zenzero, aglio, pimento, alloro, chiodi di garofano, finocchio, peperoncino, cardamomo, pepe bianco, pepe nero.',
		33,
		4.95,
		'Miscela di spezie in polvere, adatto per insaporire arrosti, verdure, etc.',
		'2023-09-30',
		'curry.jpg',
		8,
		17
	),
	(
		'Anice stellato (frutti)',
		'anice stellato.',
		75,
		6.15,
		'Anice stellato essiccato.',
		'2024-02-03',
		'anice-stellato.jpg',
		8,
		73
	),
	(
		'Origano',
		'origano.',
		57,
		4.75,
		'Origano essiccato, prodotto nel 2020.',
		'2021-11-24',
		'origano.jpg',
		8,
		23
	),
	(
		'Pepe Nero',
		'pepe.',
		28,
		3.65,
		'Pepe nero in grani essiccato, selezionato nelle migliori coltivazioni di tutto il mondo.',
		'2024-10-06',
		'pepe-nero.jpg',
		8,
		40
	),
	(
		'Anice',
		'semi di anice.',
		22,
		2.90,
		'Semi di anice essiccati, selezionati nelle migliori coltivazioni di tutto il mondo.',
		'2023-01-07',
		'anice.jpg',
		8,
		25
	),
	(
		'Gomasio',
		'semi di sesamo tostato bio, sale.',
		140,
		3.00,
		'Condimento biologico a base di sesamo tostato.',
		'2022-05-08',
		'gomasio.jpg',
		8,
		3
	),
	(
		'Menta',
		'menta.',
		20,
		4.30,
		'Menta essiccata, prodotta durante l\'estate 2019.',
		'2024-01-06',
		'menta.jpg',
		1,
		24
	),
	(
		'Curcuma',
		'curcuma.',
		100,
		4.55,
		'Curcuma essiccata e macinata, prodotta durante l\'estate 2020.',
		'2022-01-16',
		'curcuma.jpg',
		8,
		33
	),
	(
		'Zenzero',
		'zenzero.',
		30,
		5.40,
		'Zenzero essiccato e macinato, prodotta durante l\'estate 2020.',
		'2021-10-05',
		'zenzero.jpg',
		8,
		103
	),
	(
		'Noce moscata',
		'noce moscata.',
		30,
		3.70,
		'Noce moscata essiccata e macinata, prodotta durante l\'estate 2020.',
		'2021-10-01',
		'noce-moscata.jpg',
		8,
		68
	),
	(
		'Grano saraceno',
		'grano saraceno.',
		630,
		2.90,
		'Grano saraceno in chicco, tempo di cottura 20 minuti ca.',
		'2021-01-31',
		'grano-saraceno.jpg',
		4,
		36
	),
	(
		'Riso semintegrale',
		'riso semintegrale.',
		600,
		3.30,
		'Riso semiintegrale in chicco, tempo di cottura 30 minuti ca.',
		'2021-11-30',
		'riso-semintegrale.jpg',
		4,
		23
	),
	(
		'Riso venere',
		'riso venere.',
		200,
		3.40,
		'Riso venere in chicco, tempo di cottura 50 minuti ca.',
		'2022-06-30',
		'riso-venere.jpg',
		4,
		11
	),
	(
		'Marmellata di mele cotogne',
		'mele cotogne, succo di limone, zucchero di canna, acqua.',
		310,
		5.80,
		'Marmellata di mele cotogne, prodotta il 28/10/2020.',
		'2022-10-28',
		'marmellata-mele-cotogne.jpg',
		6,
		56
	),
	(
		'Marmellata di pere e zenzero',
		'pere, zenzero, zucchero di canna, succo di limone, acqua.',
		320,
		5.30,
		'Marmellata di pere e zenzero, prodotta il 10/11/2020.',
		'2022-11-10',
		'marmellata-pere-zenzero.jpg',
		6,
		13
	),
	(
		'Marmellata di pesche',
		'pesche, zucchero di canna, succo di limone.',
		300,
		5.60,
		'Marmellata di pesche, prodotta il 13/08/2020.',
		'2022-08-13',
		'marmellata-pesche.jpg',
		6,
		77
	),
	(
		'Marmellata di arance e noci',
		'arance, noci, uvetta, zucchero di canna, succo di limone.',
		120,
		6.20,
		'Marmellata di arance e noci, prodotta il 15/01/2020.',
		'2022-01-15',
		'marmellata-arance-noci.jpg',
		6,
		91
	),
	(
		'Marmellata di Mirtilli Rossi',
		'mirtilli rossi, zucchero, acqua, succo di limone concentrato, gelificante pectina.',
		600,
		8.10,
		'Composta di mirtilli rossi.',
		'2022-10-04',
		'marmellata-mirtilli-rossi.jpg',
		6,
		29
	),
	(
		'Marmellata di prugne bio',
		'prugne, zucchero di canna, succo di limone.',
		510,
		7.40,
		'Marmellata di prugne bio, prodotta il 01/09/2020.',
		'2022-09-01',
		'marmellata-prugne-bio.jpg',
		6,
		81
	),
	(
		'Pomodori pelati',
		'pomodori, sale.',
		100,
		3.00,
		'Pomodori pelati, prodotti il 10/9/2020',
		'2023-03-14',
		'pomodori-pelati.jpg',
		9,
		16
	),
	(
		'Cannella',
		'cannella.',
		45,
		1.45,
		'Cannella in polvere.',
		'2023-06-15',
		'cannella.jpg',
		8,
		74
	),
	(
		'Estratto di rapa rossa',
		'acqua, rape rosse di origine italiana, succo di limone.',
		520,
		2.50,
		'Estratto di rapa rossa non zuccherato.',
		'2023-08-26',
		'estratto-rapa-rossa.jpg',
		10,
		26
	),
	(
		'Riso integrale',
		'riso integrale.',
		750,
		3.40,
		'Riso integrale in chicco, tempo di cottura 60 minuti ca.',
		'2021-10-31',
		'riso-integrale.jpg',
		4,
		94
	),
	(
		'Quinoa',
		'quinoa.',
		560,
		5.65,
		'Quinoa, tempo di cottura 20 minuti ca.',
		'2022-02-24',
		'quinoa.jpg',
		4,
		11
	),
	(
		'Semi di sesamo',
		'semi di sesamo.',
		275,
		2.60,
		'Semi di sesamo non tostati.',
		'2021-05-28',
		'semi-sesamo.jpg',
		12,
		57
	),
	(
		'Datteri',
		'datteri.',
		300,
		5.65,
		'Datteri al naturale.',
		'2022-02-28',
		'datteri.jpg',
		11,
		20
	),
	(
		'Fave di cacao',
		'fave di cacao.',
		40,
		7.20,
		'Fave di cacao intere.',
		'2021-02-28',
		'fave-cacao.jpg',
		16,
		63
	),
	(
		'Miele di millefiori',
		'miele.',
		1000,
		6.75,
		'Miele di millefiori prodotto nella provincia di Rimini.',
		'2022-07-01',
		'miele-millefiori.jpg',
		7,
		34
	),
	(
		'Anacardi',
		'anacardi.',
		90,
		6.45,
		'Anacardi al naturale.',
		'2023-07-04',
		'anacardi.jpg',
		11,
		48
	),
	(
		'Zucchero a velo vanigliato',
		'zucchero, vaniglia.',
		110,
		7.40,
		'Zucchero a velo vanigliato.',
		'2022-09-12',
		'zucchero-a-velo-vanigliato.jpg',
		7,
		56
	),
	(
		'Uva Sultanina',
		'uva sultanina.',
		140,
		5.25,
		'Uvetta essiccata.',
		'2024-06-19',
		'uva-sultanina.jpg',
		11,
		55
	),
	(
		'Sale',
		'sale.',
		60,
		2.80,
		'Sale marino integrale dolce di Cervia.',
		'2030-01-01',
		'sale.jpg',
		13,
		25
	),
	(
		'Riso basmati',
		'riso basmati.',
		335,
		4.95,
		'Riso basmati, tempo di cottura 15 minuti ca.',
		'2023-03-25',
		'riso-basmati.jpg',
		4,
		16
	),
	(
		'Ditalini',
		'pasta di semola di grano duro.',
		420,
		4.50,
		'Pasta di semola di grano duro, tempo di cottura 8 minuti ca.',
		'2024-12-08',
		'ditalini.jpg',
		14,
		73
	),
	(
		'Riso bianco',
		'riso bianco.',
		300,
		5.25,
		'Riso bianco, tempo di cottura 15 minuti ca.',
		'2023-11-04',
		'riso-bianco.jpg',
		4,
		42
	),
	(
		'Casarecce',
		'pasta di ceci.',
		200,
		6.45,
		'Pasta di ceci, tempo di cottura 15 minuti ca.',
		'2022-04-20',
		'casarecce.jpg',
		14,
		17
	),
	(
		'Ruote',
		'pasta di semola di grano duro.',
		350,
		4.60,
		'Pasta di semola di grano duro, tempo di cottura 10 minuti ca.',
		'2023-04-10',
		'ruote.jpg',
		14,
		36
	),
	(
		'Cellentani',
		'pasta di semola di grano duro.',
		75,
		3.40,
		'Pasta di semola di grano duro, tempo di cottura 10 minuti ca.',
		'2023-01-08',
		'cellentani.jpg',
		14,
		78
	),
	(
		'Pomodori secchi',
		'pomodori, sale.',
		150,
		6.75,
		'Pomodori essiccati salati.',
		'2025-06-04',
		'pomodori-secchi.jpg',
		9,
		28
	),
	(
		'Peperoncini piccanti interi',
		'peperoncini piccanti.',
		30,
		7.70,
		'Peperoncini piccanti interi essiccati.',
		'2025-04-07',
		'peperoncini.jpg',
		8,
		97
	),
	(
		'Chiodi di garofano interi',
		'chiodi di garofano.',
		62,
		5.00,
		'Chiodi di garofano interi.',
		'2024-09-27',
		'chiodi-garofano.jpg',
		8,
		8
	),
	(
		'Lievito per dolci',
		'amido di mais biologico, agenti lievitanti: tartrato di potassio estratto dalle uve, bicarbonato.',
		26,
		5.30,
		'Lievito istantaneo in polvere per dolci e salati.',
		'2023-03-31',
		'lievito-dolci.jpg',
		15,
		5
	),
	(
		'Pepe rosa',
		'pepe rosa.',
		20,
		2.10,
		'Bacche essiccate.',
		'2021-07-31',
		'pepe-rosa.jpg',
		8,
		43
	),
	(
		'Semi di zucca',
		'semi di zucca.',
		35,
		3.85,
		'Semi di zucca interi.',
		'2021-01-07',
		'semi-zucca.jpg',
		12,
		28
	);

insert into target(id, country, province, city, address, cap) values
	(1, 'Italia', 'Rimini', 'Rimini', 'Via di Mezzo 2', 23419);

insert into seller(email, password) values
	(
		'gianpiero@productions.com', /* tastypassword */
		'$2y$10$wd/8adSGAJi9kodS06tnv.ftMBMxHmxNMf4goM4fXzR0huHpDKB1y'
	);

insert into customer
	(
		name,
		surname,
		society,
		phone,
		email,
		password,
		target
	)
	values
	(
		'Mario',
		'QuelloCheCompra',
		null,
		'3659351672',
		'thisthemario@coldmail.is', /* bruhpassword */
		'$2y$10$O82RkxziALpGUIfFpi5Qbe7313KXi4jqvqzkIgkR4rt4Kcy6TB7zG',
		1
	);

insert into discount(code, save, used) values
	('12345678', 10, 0),
	('abcdefgh', 20, 0);

insert into shipment(name, cost) values
	('Normale', 4),
	('Veloce', 8),
	('Grosse quantità', 15);

insert into payment(name) values
	('Carta di credito'),
	('PayPal'),
	('Contrassegno');

insert into notification_type(name) values
	('checkout_accepted'),
	('checkout_working'),
	('checkout_sent'),
	('product_outofstock');
