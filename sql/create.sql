create table category
(
	id	bigint unsigned	not null auto_increment primary key,
	name	varchar(64)	not null unique key
);

create table product
(
	id		bigint unsigned	not null auto_increment primary key,
	name		varchar(64)	not null,
	ingredients	varchar(1024)	not null,
	weight		int unsigned	not null,
	cost		float unsigned	not null,
	description	varchar(1024)	not null,
	deadline	datetime	not null,
	image		varchar(64)	not null,
	category	bigint unsigned	not null,
	quantity	int unsigned	not null,
	fulltext(name),
	constraint product_category
		foreign key(category)
		references category(id)
		on delete restrict
		on update restrict
);

create table target
(
	id		bigint unsigned	not null auto_increment primary key,
	country		varchar(64)	not null,
	province	varchar(64)	not null,
	city		varchar(64)	not null,
	address		varchar(64)	not null,
	cap		int unsigned	not null
);

create table seller
(
	id		bigint unsigned	not null auto_increment primary key,
	email		varchar(64)	not null unique key,
	password	char(60)	not null /* bcrypt */
);

create table customer
(
	id		bigint unsigned	not null auto_increment primary key,
	name		varchar(16)	not null,
	surname		varchar(16)	not null,
	society		varchar(64),
	phone		int unsigned,
	email		varchar(64)	not null unique key,
	password	char(60)	not null, /* bcrypt */
	target		bigint unsigned	not null,
	constraint customer_target
		foreign key(target)
		references target(id)
		on delete restrict
		on update restrict
);

create table discount
(
	id	bigint unsigned	not null auto_increment primary key,
	code	char(8)		not null unique key,
	save	float unsigned	not null,
	used	boolean		not null,
	constraint code_size
		check (char_length(code) = 8)
);

create table shipment
(
	id	bigint unsigned	not null auto_increment primary key,
	name	varchar(16)	not null unique key,
	cost	float unsigned	not null
);

create table payment
(
	id	bigint unsigned	not null auto_increment primary key,
	name	varchar(16)	not null unique key
);

create table checkout
(
	id		bigint unsigned		not null auto_increment primary key,
	accepted	datetime		not null,
	working		datetime,
	sent		datetime,
	customer	bigint unsigned		not null,
	target		bigint unsigned		not null,
	discount	bigint unsigned,
	shipment	bigint unsigned		not null,
	payment		bigint unsigned		not null,
	vat		tinyint unsigned	not null,
	constraint checkout_customer
		foreign key(customer)
		references customer(id)
		on delete restrict
		on update restrict,
	constraint checkout_target
		foreign key(target)
		references target(id)
		on delete restrict
		on update restrict,
	constraint checkout_discount
		foreign key(discount)
		references discount(id)
		on delete restrict
		on update restrict,
	constraint checkout_shipment
		foreign key(shipment)
		references shipment(id)
		on delete restrict
		on update restrict,
	constraint checkout_payment
		foreign key(payment)
		references payment(id)
		on delete restrict
		on update restrict,
	constraint vat_percentage
		check(vat >= 0 and vat <= 100)
);

create table checkout_detail
(
	checkout	bigint unsigned		not null,
	product		bigint unsigned		not null,
	quantity	tinyint unsigned	not null,
	primary key(checkout, product),
	constraint checkout_detail_checkout
		foreign key(checkout)
		references checkout(id)
		on delete restrict
		on update restrict,
	constraint checkout_detail_product
		foreign key(product)
		references product(id)
		on delete restrict
		on update restrict
);

create table notification_type
(
	id	bigint unsigned	not null auto_increment primary key,
	name	varchar(64)	not null unique key
);

create table notification
(
	id		bigint unsigned	not null auto_increment primary key,
	customer	bigint unsigned,
	seller		bigint unsigned,
	checkout	bigint unsigned,
	product		bigint unsigned,
	type		bigint unsigned	not null,
	seen		boolean		not null,
	constraint notification_customer
		foreign key(customer)
		references customer(id)
		on delete restrict
		on update restrict,
	constraint notification_seller
		foreign key(seller)
		references seller(id)
		on delete restrict
		on update restrict,
	constraint notification_checkout
		foreign key(checkout)
		references checkout(id)
		on delete restrict
		on update restrict,
	constraint notification_product
		foreign key(product)
		references product(id)
		on delete restrict
		on update restrict,
	constraint notification_type
		foreign key(type)
		references notification_type(id)
		on delete restrict
		on update restrict
);
