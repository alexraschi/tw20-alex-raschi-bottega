<?php
declare(strict_types = 1);

function
get_navlink(string $name, string $target, bool $current)
{
	$navlink['name'] = $name;
	$navlink['target'] = $target;
	$navlink['current'] = $current;
	require get_page_path('navlink');
}

function
is_nav(array $skel, string $nav): bool
{
	return isset($skel['nav'][$nav]);
}

function
get_skel(object $database, string $title, ?string $highlight, string $main, array $styles, array $scripts): array
{
	$skel['title'] = $title;

	$skel['styles'] = array();
	foreach ($styles as $style)
	{
		$skel['styles'][] = get_style_path($style);
	}

	$skel['scripts'] = array();
	foreach ($scripts as $script)
	{
		$skel['scripts'][] = get_script_path($script);
	}

	if (is_value_not_null($highlight))
	{
		$skel['nav'][$highlight] = true;
	}

	$skel['categories'] = select_categories($database);

	$skel['loggedin'] = is_logged_in();
	$skel['customer'] = is_customer_logged_in();
	$skel['seller'] = is_seller_logged_in();
	if ($skel['customer'])
	{
		if (is_session('cart'))
		{
			$skel['cart'] = count($_SESSION['cart']);
		}
		$skel['notifications'] = count_unseen_notifications_by_customer_id($database, $_SESSION['id']);
	}
	else if ($skel['seller'])
	{
		$skel['notifications'] = count_unseen_notifications_by_seller_id($database, $_SESSION['id']);
	}

	foreach (array('cart', 'notifications') as $variable)
	{
		if (!isset($skel[$variable]) || $skel[$variable] === 0)
		{
			$skel[$variable] = null;
		}
	}

	$skel['main'] = get_page_path($main);
	return $skel;
}

function
get_skel_simple(object $database, string $title, bool $highlight, string $main): array
{
	if ($highlight)
	{
		$highlight = $main;
	}
	else
	{
		$highlight = null;
	}
	return get_skel($database, $title, $highlight, $main, array(), array());
}

function
get_page(object $database, string $title, bool $highlight, string $main)
{
	$skel = get_skel_simple($database, $title, $highlight, $main);
	require get_page_path('skel');
}

function
get_list(array $products, string $prefix, int $limit, int $offset, int $count)
{
	$list['products'] = $products;
	$list['prefix'] = $prefix;
	$list['limit'] = $limit;
	$list['offset'] = $offset;
	$list['count'] = $count;
	require get_page_path('list');
}
