<?php
declare(strict_types = 1);

function
is_get(string $variable): bool
{
	return isset($_GET[$variable]);
}

function
is_get_not_empty(string $variable): bool
{
	return is_get($variable) && is_string_not_empty($_GET[$variable]);
}

function
default_get(string $variable, mixed $value): string
{
	if (is_get($variable))
	{
		return $_GET[$variable];
	}
	else
	{
		return (string)$value;
	}
}

function
is_post(string $variable): bool
{
	return isset($_POST[$variable]);
}

function
is_post_not_empty(string $variable): bool
{
	return is_post($variable) && is_string_not_empty($_POST[$variable]);
}

function
are_post(array $variables): bool
{
	foreach ($variables as $variable)
	{
		if (!is_post($variable))
		{
			return false;
		}
	}
	return true;
}

function
are_post_not_empty(array $variables): bool
{
	foreach ($variables as $variable)
	{
		if (!is_post_not_empty($variable))
		{
			return false;
		}
	}
	return true;
}

function
is_session(string $variable): bool
{
	return isset($_SESSION[$variable]);
}

function
is_session_not_empty(string $variable): bool
{
	return is_session($variable) && is_string_not_empty($_SESSION[$variable]);
}

function
are_session(array $variables): bool
{
	foreach ($variables as $variable)
	{
		if (!is_session($variable))
		{
			return false;
		}
	}
	return true;
}

function
prepend_space_if_not_empty(string $string): string
{
	if (is_string_not_empty($string))
	{
		return ' ' . $string;
	}
	else
	{
		return '';
	}
}

function
append_space_if_not_empty(string $string): string
{
	if (is_string_not_empty($string))
	{
		return $string . ' ';
	}
	else
	{
		return '';
	}
}

function
is_logged_in(): bool
{
	return is_session('id');
}

function
is_customer(): bool
{
	return $_SESSION['customer'];
}

function
is_seller(): bool
{
	return !is_customer();
}

function
is_customer_logged_in(): bool
{
	return is_logged_in() && is_customer();
}

function
is_seller_logged_in(): bool
{
	return is_logged_in() && is_seller();
}

function
login(int $id, bool $customer)
{
	$_SESSION['id'] = $id;
	$_SESSION['customer'] = $customer;
}

function
logout()
{
	unset($_SESSION['id']);
	unset($_SESSION['customer']);
}

function
clear_cart()
{
	unset($_SESSION['cart']);
	unset($_SESSION['discount']);
	unset($_SESSION['shipment']);
}

function
multi_define_with_prefix(string $prefix, array $variables)
{
	$index = 0;
	foreach ($variables as $variable)
	{
		define($prefix . $variable, $index++);
	}
}

function
redirect_to(string $destination)
{
	header('Location: ' . $destination);
	exit;
}

function
float_format(float $input): string
{
	return number_format($input, 2, '.', '');
}

/* exploit strict_types to implement some functions that resemble the stricter
   version of empty() */
function
is_array_empty(array $input): bool
{
	return $input === array();
}

function
is_array_not_empty(array $input): bool
{
	return !is_array_empty($input);
}

function
is_string_empty(string $input): bool
{
	return $input === '';
}

function
is_string_not_empty(string $input): bool
{
	return !is_string_empty($input);
}

function
is_value_null(mixed $input): bool
{
	return $input === null;
}

function
is_value_not_null(mixed $input): bool
{
	return !is_value_null($input);
}

function
string_to_float(string $input): float
{
	$ints = explode('.', $input);
	$count = count($ints);
	if ($count === 1 || $count === 2)
	{
		foreach ($ints as $int)
		{
			if (!ctype_digit($int))
			{
				die($input . ' not a float');
			}
		}
	}
	else
	{
		die($input . ' not a float');
	}
	return (float)$input;
}

function
string_to_nullint(string $input): ?int
{
	if (ctype_digit($input))
	{
		return (int)$input;
	}
	else
	{
		return null;
	}
}

function
string_to_int(string $input): int
{
	$result = string_to_nullint($input);
	if (is_value_null($result))
	{
		die($input . ' not an int');
	}
	else
	{
		return $result;
	}
}

function
string_to_nullstring(string $input): ?string
{
	if (is_string_empty($input))
	{
		return null;
	}
	else
	{
		return $input;
	}
}

function
to_string(mixed $input): string
{
	return (string)$input;
}
