<?php
declare(strict_types = 1);

function
database_connect(string $server, string $user, string $password, string $name, string $lang): mysqli
{
	$database = mysqli_connect($server, $user, $password, $name);
	if (is_bool($database) && $database === false)
	{
		die('connection error: ' . mysqli_connect_error());
	}
	else
	{
		mysqli_query($database, 'set lc_messages = it_IT');
		$query = mysqli_prepare($database, <<< 'EOF'
			set lc_messages = ?
			EOF);
		mysqli_stmt_bind_param($query, 's', $lang);
		mysqli_stmt_execute($query);
		return $database;
	}
}

function
array_array_from(mysqli_stmt $query): array
{
	if (!mysqli_stmt_execute($query))
	{
		return array();
	}
	else
	{
		return mysqli_fetch_all(mysqli_stmt_get_result($query), MYSQLI_ASSOC);
	}
}

function
single_array_from(mysqli_stmt $query): array
{
	$array = array_array_from($query);
	if (is_array_empty($array))
	{
		return array();
	}
	else
	{
		return $array[0];
	}
}

function
single_string_from(mysqli_stmt $query, string $variable): ?string
{
	$array = single_array_from($query);
	if (is_array_empty($array))
	{
		return null;
	}
	else
	{
		return $array[$variable];
	}
}

function
single_int_from(mysqli_stmt $query, string $variable): ?int
{
	$array = single_array_from($query);
	if (is_array_empty($array))
	{
		return null;
	}
	else
	{
		return $array[$variable];
	}
}

function
start_transaction(mysqli $link): bool
{
	return mysqli_query($link, 'start transaction with consistent snapshot');
}

function
rollback_transaction(mysqli $link): string
{
	$error = mysqli_error($link);
	mysqli_query($link, 'rollback');
	return $error;
}

function
commit_transaction(mysqli $link): ?string
{
	if (!mysqli_query($link, 'commit'))
	{
		return mysqli_error($link);
	}
	else
	{
		return null;
	}
}

function
error_string_on_failure(mysqli $link, mysqli_stmt $query): ?string
{
	if (!mysqli_stmt_execute($query))
	{
		return mysqli_error($link);
	}
	else
	{
		return null;
	}
}

function
verify_password(mysqli_stmt $query, string $password): ?int
{
	$result = single_array_from($query);
	if (is_array_not_empty($result) && password_verify($password, $result['password']))
	{
		return $result['id'];
	}
	else
	{
		return null;
	}
}

function
select_categories(mysqli $link): array
{
	return array_array_from(mysqli_prepare($link, <<< 'EOF'
		select id, name
		from category
		order by name
		EOF));
}

function
select_category_name_by_id(mysqli $link, int $id): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select name
		from category
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_string_from($query, 'name');
}

function
count_products_by_category_id(mysqli $link, int $id): int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from product
		where category = ?
		and quantity > 0
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_int_from($query, 'count(id)');
}

function
select_products_by_category_id(mysqli $link, int $id, int $limit, int $offset): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, name, image, cost, description
		from product
		where category = ?
		and quantity > 0
		order by name
		limit ?
		offset ?
		EOF);
	mysqli_stmt_bind_param($query, 'iii', $id, $limit, $offset);
	return array_array_from($query);
}

function
select_product_by_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select name, ingredients, weight, cost, description, deadline, image, quantity
		from product
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_array_from($query);
}

function
count_products_by_name_match(mysqli $link, string $match): int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from product
		where match(name) against(? in boolean mode)
		and quantity > 0
		EOF);
	mysqli_stmt_bind_param($query, 's', $match);
	return single_int_from($query, 'count(id)');
}

function
select_products_by_name_match(mysqli $link, string $string, int $limit, int $offset): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, name, image, cost, description
		from product
		where match(name) against(? in boolean mode)
		and quantity > 0
		order by name
		limit ?
		offset ?
		EOF);
	mysqli_stmt_bind_param($query, 'sii', $string, $limit, $offset);
	return array_array_from($query);
}

function
select_customer_id_by_login(mysqli $link, string $email, string $password): ?int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, password
		from customer
		where email = ?
		EOF);
	mysqli_stmt_bind_param($query, 's', $email);
	return verify_password($query, $password);
}

function
select_seller_id_by_login(mysqli $link, string $email, string $password): ?int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, password
		from seller
		where email = ?
		EOF);
	mysqli_stmt_bind_param($query, 's', $email);
	return verify_password($query, $password);
}

function
select_discount_id_by_code(mysqli $link, string $code): ?int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id
		from discount
		where code = ? and used = 0
		EOF);
	mysqli_stmt_bind_param($query, 's', $code);
	return single_int_from($query, 'id');
}

function
select_shipments(mysqli $link): array
{
	return array_array_from(mysqli_prepare($link, <<< 'EOF'
		select id, name, cost
		from shipment
		order by cost
		EOF));
}

function
select_payments(mysqli $link): array
{
	return array_array_from(mysqli_prepare($link, <<< 'EOF'
		select id, name
		from payment
		order by name
		EOF));
}

function
select_target_id_by_customer_id(mysqli $link, int $id): int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select target.id as id
		from target
		inner join customer
		on customer.target = target.id
		where customer.id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_int_from($query, 'id');
}

/* last_insert_id() is connection specific, there are no race conditions */
/* https://mariadb.com/kb/en/auto_increment-faq/#what-if-someone-else-inserts-before-i-select-my-id */
/* in practise these 2 cannot return null but single_int_from can */
function
select_last_target_id(mysqli $link): int
{
	return single_int_from(mysqli_prepare($link, <<< 'EOF'
		select last_insert_id()
		from target
		EOF), 'last_insert_id()');
}

function
select_last_checkout_id(mysqli $link): int
{
	return single_int_from(mysqli_prepare($link, <<< 'EOF'
		select last_insert_id()
		from checkout
		EOF), 'last_insert_id()');
}

function
insert_target(mysqli $link, string $country, string $provice, string $city, string $address, int $cap): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		insert into target(country, province, city, address, cap)
		values(?, ?, ?, ?, ?)
		EOF);
	mysqli_stmt_bind_param($query, 'ssssi', $country, $provice, $city, $address, $cap);
	return error_string_on_failure($link, $query);
}

function
insert_customer(mysqli $link, string $name, string $surname, ?string $society, ?int $phone, string $email, string $password, string $country, string $provice, string $city, string $address, int $cap): ?string
{
	if (!start_transaction($link))
	{
		return mysqli_error($link);
	}

	$message = insert_target($link, $country, $provice, $city, $address, $cap);

	if (is_value_not_null($message))
	{
		return rollback_transaction($link);
	}

	$target = select_last_target_id($link);

	$hash = password_hash($password, PASSWORD_HASH);
	$query = mysqli_prepare($link, <<< 'EOF'
		insert into customer(name, surname, society, phone, email, password, target)
		values(?, ?, ?, ?, ?, ?, ?)
		EOF);
	mysqli_stmt_bind_param($query, 'sssissi', $name, $surname, $society, $phone, $email, $hash, $target);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	return commit_transaction($link);
}

function
insert_checkout(mysqli $link, int $customer, int $target, ?int $discount, int $shipment, int $payment, int $vat, array $products): ?string
{
	if (!start_transaction($link))
	{
		return mysqli_error($link);
	}

	if (is_value_not_null($discount))
	{
		$query = mysqli_prepare($link, <<< 'EOF'
			update discount
			set used = 1
			where id = ?
			EOF);
		mysqli_stmt_bind_param($query, 'i', $discount);

		if (!mysqli_stmt_execute($query))
		{
			return rollback_transaction($link);
		}
	}

	$query = mysqli_prepare($link, <<< 'EOF'
		insert into checkout(accepted, working, sent, customer, target, discount, shipment, payment, vat)
		values(now(), null, null, ?, ?, ?, ?, ?, ?)
		EOF);
	mysqli_stmt_bind_param($query, 'iiiiii', $customer, $target, $discount, $shipment, $payment, $vat);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	$checkout = select_last_checkout_id($link);

	$sellers = array_array_from(mysqli_prepare($link, <<< 'EOF'
		select id
		from seller
		EOF));

	$query = mysqli_prepare($link, <<< 'EOF'
		select id
		from notification_type
		where name = 'product_outofstock'
		EOF);
	$type = single_int_from($query, 'id');

	if (is_value_null($type))
	{
		return rollback_transaction($link);
	}

	foreach ($products as $product => $quantity)
	{
		$query = mysqli_prepare($link, <<< 'EOF'
			insert into checkout_detail(checkout, product, quantity)
			values(?, ?, ?)
			EOF);
		mysqli_stmt_bind_param($query, 'iii', $checkout, $product, $quantity);

		if (!mysqli_stmt_execute($query))
		{
			return rollback_transaction($link);
		}

		$query = mysqli_prepare($link, <<< 'EOF'
			update product
			set quantity = quantity - ?
			where id = ?
			EOF);
		mysqli_stmt_bind_param($query, 'ii', $quantity, $product);

		if (!mysqli_stmt_execute($query))
		{
			return rollback_transaction($link);
		}

		$query = mysqli_prepare($link, <<< 'EOF'
			select id
			from product
			where id = ?
			and quantity = 0
			EOF);
		mysqli_stmt_bind_param($query, 'i', $product);

		if (single_int_from($query, 'id') === $product)
		{
			foreach ($sellers as $seller)
			{
				$query = mysqli_prepare($link, <<< 'EOF'
					insert into notification(customer, seller, checkout, product, type, seen)
					values(null, ?, ?, ?, ?, 0)
					EOF);
				mysqli_stmt_bind_param($query, 'iiii', $seller['id'], $checkout, $product, $type);

				if (!mysqli_stmt_execute($query))
				{
					return rollback_transaction($link);
				}
			}
		}
	}

	$query = mysqli_prepare($link, <<< 'EOF'
		select id
		from notification_type
		where name = 'checkout_accepted'
		EOF);
	$type = single_int_from($query, 'id');

	if (is_value_null($type))
	{
		return rollback_transaction($link);
	}

	$query = mysqli_prepare($link, <<< 'EOF'
		insert into notification(customer, seller, checkout, product, type, seen)
		values(?, null, ?, null, ?, 0)
		EOF);
	mysqli_stmt_bind_param($query, 'iii', $customer, $checkout, $type);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	return commit_transaction($link);
}

function
select_checkouts_by_customer_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, accepted, working, sent
		from checkout
		where customer = ?
		order by id desc
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return array_array_from($query);
}

function
select_checkout_by_id(mysqli $link, int $id, int $customer): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select
			checkout.accepted	as accepted,
			checkout.working	as working,
			checkout.sent		as sent,
			checkout.vat		as vat,
			target.country		as country,
			target.province		as province,
			target.city		as city,
			target.address		as address,
			target.cap		as cap,
			discount.save		as save,
			shipment.name		as shipment,
			shipment.cost		as shipment_cost,
			payment.name		as payment,
			(sum(checkout_detail.quantity * product.cost) - ifnull(discount.save, 0) + shipment.cost)	as partial
		from checkout
		inner join checkout_detail
		on checkout.id = checkout_detail.checkout
		inner join product
		on checkout_detail.product = product.id
		inner join target
		on checkout.target = target.id
		left join discount
		on checkout.discount = discount.id
		inner join shipment
		on checkout.shipment = shipment.id
		inner join payment
		on checkout.payment = payment.id
		where checkout.id = ?
		and checkout.customer = ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $id, $customer);
	return single_array_from($query);
}

function
select_checkout_products_by_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select
			detail.quantity		as quantity,
			product.id		as id,
			product.name		as name,
			product.cost		as cost,
			product.description	as description
		from checkout
		inner join checkout_detail as detail
		on checkout.id = detail.checkout
		inner join product
		on detail.product = product.id
		where checkout.id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return array_array_from($query);
}

function
select_discount_by_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select code, save
		from discount
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_array_from($query);
}

function
insert_category(mysqli $link, string $name): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		insert into category(name)
		values(?)
		EOF);
	mysqli_stmt_bind_param($query, 's', $name);
	return error_string_on_failure($link, $query);
}

function
insert_product(mysqli $link, string $name, string $ingredients, int $weight, float $cost, string $description, string $deadline, string $image, int $category, int $quantity): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		insert into product(name, ingredients, weight, cost, description, deadline, image, category, quantity)
		values(?, ?, ?, ?, ?, ?, ?, ?, ?)
		EOF);
	mysqli_stmt_bind_param($query, 'ssidsssii', $name, $ingredients, $weight, $cost, $description, $deadline, $image, $category, $quantity);
	return error_string_on_failure($link, $query);
}

function
select_customer_by_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select name, surname, society, phone, email, password
		from customer
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_array_from($query);
}

function
select_target_by_customer_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select
			target.country	as country,
			target.province	as province,
			target.city	as city,
			target.address	as address,
			target.cap	as cap
		from customer
		inner join target
		on customer.target = target.id
		where customer.id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_array_from($query);
}

function
update_customer_by_id(mysqli $link, int $id, string $name, string $surname, ?string $society, ?int $phone, string $email): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update customer
		set
			name = ?,
			surname = ?,
			society = ?,
			phone = ?,
			email = ?
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'sssisi', $name, $surname, $society, $phone, $email, $id);
	return error_string_on_failure($link, $query);
}

function
update_customer_password_by_id(mysqli $link, int $id, string $old_password, string $new_password): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select password
		from customer
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	$password = single_string_from($query, 'password');

	if (is_value_null($password))
	{
		return mysqli_error($link);
	}

	if (!password_verify($old_password, $password))
	{
		return 'verifica della vecchia password fallita';
	}

	$hash = password_hash($new_password, PASSWORD_HASH);
	$query = mysqli_prepare($link, <<< 'EOF'
		update customer
		set password = ?
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'si', $hash, $id);

	return error_string_on_failure($link, $query);
}

function
update_target_by_customer_id(mysqli $link, int $id, string $country, string $province, string $city, string $address, int $cap): ?string
{
	if (!start_transaction($link))
	{
		return mysqli_error($link);
	}

	$result = insert_target($link, $country, $province, $city, $address, $cap);

	if (is_value_not_null($result))
	{
		return rollback_transaction($link); /* error hasn't changed so $result === mysqli_error($link) */
	}

	$target = select_last_target_id($link);
	$query = mysqli_prepare($link, <<< 'EOF'
		update customer
		set target = ?
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $target, $id);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	return commit_transaction($link);
}

function
count_unseen_notifications_by_customer_id(mysqli $link, int $id): int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from notification
		where customer = ?
		and seen = 0
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_int_from($query, 'count(id)');
}

function
count_unseen_notifications_by_seller_id(mysqli $link, int $id): int
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from notification
		where seller = ?
		and seen = 0
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return single_int_from($query, 'count(id)');
}

function
select_notifications_by_customer_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select
			notification.id		as id,
			notification.checkout	as checkout,
			notification_type.name	as type,
			notification.seen	as seen
		from notification
		inner join notification_type
		on notification.type = notification_type.id
		where notification.customer = ?
		order by notification.id desc
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return array_array_from($query);
}

function
update_customer_notification_by_id(mysqli $link, int $id, int $customer): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update notification
		set seen = 1
		where id = ?
		and customer = ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $id, $customer);
	return error_string_on_failure($link, $query);
}

function
update_seller_notification_by_id(mysqli $link, int $id, int $seller): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update notification
		set seen = 1
		where id = ?
		and seller = ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $id, $seller);
	return error_string_on_failure($link, $query);
}

function
select_products(mysqli $link, int $limit, int $offset): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, name, image, cost, description
		from product
		where quantity > 0
		order by name
		limit ?
		offset ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $limit, $offset);
	return array_array_from($query);
}

function
count_products(mysqli $link): int
{
	return single_int_from(mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from product
		where quantity > 0
		EOF), 'count(id)');
}

function
insert_discount(mysqli $link, string $code, float $save): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		insert into discount(code, save, used)
		values(?, ?, 0)
		EOF);
	mysqli_stmt_bind_param($query, 'sd', $code, $save);
	return error_string_on_failure($link, $query);
}

function
select_checkouts(mysqli $link): array
{
	return array_array_from(mysqli_prepare($link, <<< 'EOF'
		select id, accepted, working, sent
		from checkout
		order by id desc
		EOF));
}

function
update_checkout_state_by_id(mysqli $link, int $id, int $state): ?string
{
	if ($state === CHECKOUT_STATE_WORKING)
	{
		$state = 'working';
	}
	else if ($state === CHECKOUT_STATE_SENT)
	{
		$state = 'sent';
	}
	else
	{
		return 'error: invalid state';
	}

	if (!start_transaction($link))
	{
		return mysqli_error($link);
	}

	$query = mysqli_prepare($link, <<< "EOF"
		update checkout
		set `$state` = now()
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	$query = mysqli_prepare($link, <<< 'EOF'
		select customer
		from checkout
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	$customer = single_int_from($query, 'customer');

	if (is_value_null($customer))
	{
		return rollback_transaction($link);
	}

	$name = 'checkout_' . $state;
	$query = mysqli_prepare($link, <<< 'EOF'
		select id
		from notification_type
		where name = ?
		EOF);
	mysqli_stmt_bind_param($query, 's', $name);
	$type = single_int_from($query, 'id');

	if (is_value_null($type))
	{
		return rollback_transaction($link);
	}

	$query = mysqli_prepare($link, <<< 'EOF'
		insert into notification(customer, seller, checkout, product, type, seen)
		values(?, null, ?, null, ?, 0)
		EOF);
	mysqli_stmt_bind_param($query, 'iii', $customer, $id, $type);

	if (!mysqli_stmt_execute($query))
	{
		return rollback_transaction($link);
	}

	return commit_transaction($link);
}

function
select_outofstock_products(mysqli $link, int $limit, int $offset): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select id, name, image, cost, description
		from product
		where quantity = 0
		order by name
		limit ?
		offset ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $limit, $offset);
	return array_array_from($query);
}

function
count_outofstock_products(mysqli $link): int
{
	return single_int_from(mysqli_prepare($link, <<< 'EOF'
		select count(id)
		from product
		where quantity = 0
		EOF), 'count(id)');
}

function
update_product_quantity_by_id(mysqli $link, int $id, int $quantity): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update product
		set quantity = ?
		where id = ?
		EOF);
	mysqli_stmt_bind_param($query, 'ii', $quantity, $id);
	return error_string_on_failure($link, $query);
}

function
select_notifications_by_seller_id(mysqli $link, int $id): array
{
	$query = mysqli_prepare($link, <<< 'EOF'
		select
			notification.id		as id,
			notification.product	as product,
			notification_type.name	as type,
			notification.seen	as seen,
			product.name		as product_name
		from notification
		inner join notification_type
		on notification.type = notification_type.id
		inner join product
		on notification.product = product.id
		where notification.seller = ?
		order by notification.id desc
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return array_array_from($query);
}

function
update_unseen_notifications_by_customer_id(mysqli $link, int $id): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update notification
		set seen = 1
		where customer = ?
		and seen = 0
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return error_string_on_failure($link, $query);
}

function
update_unseen_notifications_by_seller_id(mysqli $link, int $id): ?string
{
	$query = mysqli_prepare($link, <<< 'EOF'
		update notification
		set seen = 1
		where seller = ?
		and seen = 0
		EOF);
	mysqli_stmt_bind_param($query, 'i', $id);
	return error_string_on_failure($link, $query);
}

function
select_most_expensive_shipment(mysqli $link): int
{
	return single_int_from(mysqli_prepare($link, <<< 'EOF'
		select id
		from shipment
		where cost = (
			select max(cost)
			from shipment);
		EOF), 'id');
}

