<?php
declare(strict_types = 1);

function
get_checkbox_form(string $id, string $label, string $attr, bool $fake)
{
	$checkbox['id'] = $id;
	$checkbox['attr'] = append_space_if_not_empty($attr);
	if (!$fake)
	{
		$checkbox['attr'] .= 'name="' . $id . '" value="true" ';
		if (is_post($id))
		{
			$checkbox['attr'] .= 'checked ';
		}
	}
	$checkbox['label'] = $label;
	require get_page_path('form/checkbox');
}

function
get_input_form(string $id, bool $hidelabel, string $label, string $type, string $placeholder, string $attr)
{
	$input['id'] = $id;
	$input['hidelabel'] = $hidelabel;
	$input['label'] = $label;
	$input['type'] = $type;
	$input['placeholder'] = $placeholder;
	if (is_post($id))
	{
		$input['value'] = $_POST[$id];
	}
	else
	{
		$input['value'] = '';
	}
	$input['attr'] = append_space_if_not_empty($attr);
	if ($type === 'file')
	{
		require get_page_path('form/file');
	}
	else
	{
		require get_page_path('form/input');
	}
}

function
get_radio_form(string $id, string $legend, array $options, int $checked, string $class, string $attr)
{
	$radio['id'] = $id;
	$radio['legend'] = $legend;
	$radio['options'] = $options;
	$radio['checked'] = $checked;
	$radio['class'] = prepend_space_if_not_empty($class);
	$radio['attr'] = append_space_if_not_empty($attr);
	require get_page_path('form/radio');
}

function
get_select_form(string $id, bool $hidelabel, string $label, string $class, bool $empty, array $options, string $attr)
{
	$select['id'] = $id;
	$select['hidelabel'] = $hidelabel;
	$select['label'] = $label;
	$select['class'] = prepend_space_if_not_empty($class);
	$select['empty'] = $empty;
	$select['options'] = $options;
	if (is_post($id))
	{
		$select['value'] = $_POST[$id];
	}
	else
	{
		$select['value'] = '';
	}
	$select['attr'] = prepend_space_if_not_empty($attr);
	require get_page_path('form/select');
}

function
get_textarea_form(string $id, string $label, string $placeholder, string $attr)
{
	$textarea['id'] = $id;
	$textarea['label'] = $label;
	$textarea['placeholder'] = $placeholder;
	if (is_post($id))
	{
		$textarea['value'] = $_POST[$id];
	}
	else
	{
		$textarea['value'] = '';
	}
	$textarea['attr'] = prepend_space_if_not_empty($attr);
	require get_page_path('form/textarea');
}
