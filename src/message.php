<?php
declare(strict_types = 1);

function
get_error_message(string $text)
{
	$message['type'] = 'danger';
	$message['text'] = $text;
	require get_page_path('message');
}

function
get_success_message(string $text)
{
	$message['type'] = 'success';
	$message['text'] = $text;
	require get_page_path('message');
}
