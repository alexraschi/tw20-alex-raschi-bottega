<?php
declare(strict_types = 1);

function
get_home_page(object $database)
{
	get_page($database, 'Home', true, 'home');
}

function
get_category_page(object $database)
{
	multi_define_with_prefix('CATEGORY_', array(
		'DEFAULT',
		'NOT_FOUND',
		'EMPTY',
		'FROM_LOGIN',
		'FROM_CART_END'));

	$category['limit'] = string_to_int(default_get('limit', PAGE_LIMIT));
	$category['offset'] = string_to_int(default_get('offset', 0));
	$category['prefix'] = 'category.php?';

	if (is_get('id'))
	{
		$id = string_to_nullint($_GET['id']);

		if (is_value_not_null($id))
		{
			$name = select_category_name_by_id($database, $id);

			$category['prefix'] .= 'id=' . $id;

			$category['products'] = select_products_by_category_id(
				$database,
				$id,
				$category['limit'],
				$category['offset']);

			$category['count'] = count_products_by_category_id($database, $id);
		}
	}
	else
	{
		$name = 'Tutti i prodotti';

		$category['products'] = select_products(
			$database,
			$category['limit'],
			$category['offset']);

		$category['count'] = count_products($database);
	}

	if (is_value_null($name))
	{
		$name = 'Categoria non trovata';
		$category['status'] = CATEGORY_NOT_FOUND;
	}
	else if (is_array_empty($category['products']))
	{
		$category['status'] = CATEGORY_EMPTY;
	}
	else
	{
		if (is_session_not_empty('from'))
		{
			if ($_SESSION['from'] === 'login')
			{
				$category['status'] = CATEGORY_FROM_LOGIN;
			}
			else if ($_SESSION['from'] === 'cart_end')
			{
				$category['status'] = CATEGORY_FROM_CART_END;
			}

			unset($_SESSION['from']);
		}
		else
		{
			$category['status'] = CATEGORY_DEFAULT;
		}
	}

	$skel = get_skel(
		$database,
		$name,
		'categories',
		'category',
		array(),
		array());

	require get_page_path('skel');
}

function
get_outofstock_page(object $database)
{
	multi_define_with_prefix('OUTOFSTOCK_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'EMPTY'));

	if (!is_seller_logged_in())
	{
		$outofstock['status'] = OUTOFSTOCK_LOGIN_ERROR;
	}
	else
	{
		$outofstock['limit'] = string_to_int(default_get('limit', PAGE_LIMIT));
		$outofstock['offset'] = string_to_int(default_get('offset', 0));
		$outofstock['products'] = select_outofstock_products(
			$database,
			$outofstock['limit'],
			$outofstock['offset']);

		if (is_array_empty($outofstock['products']))
		{
			$outofstock['status'] = OUTOFSTOCK_EMPTY;
		}
		else
		{
			$outofstock['prefix'] = 'outofstock.php?';
			$outofstock['count'] = count_outofstock_products($database);
			$outofstock['status'] = OUTOFSTOCK_DEFAULT;
		}
	}

	$skel = get_skel(
		$database,
		'Prodottti esauriti',
		'categories',
		'outofstock',
		array(),
		array());

	require get_page_path('skel');
}

function
get_cart_page(object $database)
{
	multi_define_with_prefix('CART_', array(
		'DEFAULT',
		'FROM_ADDED_PRODUCT',
		'FROM_UPDATED_PRODUCT',
		'LOGIN_ERROR',
		'DISCOUNT_ERROR',
		'DISCOUNT_ADDED',
		'PRODUCT_REMOVED',
		'PRODUCT_UPDATED',
		'CLEARED',
		'EMPTY'));

	$shipments = select_shipments($database);

	if (!is_session('discount'))
	{
		$_SESSION['discount'] = null;
	}

	if (!is_session('shipment'))
	{
		$_SESSION['shipment'] = $shipments[0]['id'];
	}

	if (!is_customer_logged_in())
	{
		$cart['status'] = CART_LOGIN_ERROR;
	}
	else
	{
		$cart['discount_length'] = DISCOUNT_LENGTH;

		if (is_post_not_empty('discount'))
		{
			$discount = select_discount_id_by_code($database, $_POST['discount']);
			if (is_value_null($discount))
			{
				$cart['status'] = CART_DISCOUNT_ERROR;
			}
			else
			{
				$_SESSION['discount'] = $discount;
				$cart['status'] = CART_DISCOUNT_ADDED;
			}
		}
		else if (is_post_not_empty('shipment'))
		{
			$_SESSION['shipment'] = string_to_int($_POST['shipment']);
			$cart['status'] = CART_DEFAULT;
		}
		else if (is_post('clear'))
		{
			clear_cart();
			$cart['status'] = CART_CLEARED;
		}
		else
		{
			if (is_session_not_empty('from'))
			{
				if ($_SESSION['from'] === 'added_product')
				{
					$cart['status'] = CART_FROM_ADDED_PRODUCT;
				}
				else if ($_SESSION['from'] === 'updated_product')
				{
					$cart['status'] = CART_FROM_UPDATED_PRODUCT;
				}

				unset($_SESSION['from']);
			}
			else
			{
				$cart['status'] = CART_DEFAULT;

				if (is_session('cart'))
				{
					foreach ($_SESSION['cart'] as $id => $quantity)
					{
						$variable = 'product-' . to_string($id);

						if ($cart['status'] === CART_DEFAULT && is_post_not_empty($variable))
						{
							$value = string_to_int($_POST[$variable]);

							if ($value === 0)
							{
								unset($_SESSION['cart'][$id]);
								$cart['status'] = CART_PRODUCT_REMOVED;
							}
							else
							{
								$_SESSION['cart'][$id] = $value;
								$cart['status'] = CART_PRODUCT_UPDATED;
							}
						}
					}
				}
			}
		}

		foreach ($shipments as $shipment)
		{
			if (!is_session('shipment'))
			{
				$_SESSION['shipment'] = $shipment['id'];
			}
			$cart['shipments'][$shipment['id']] = $shipment;
		}

		$cart['products'] = array();
		$cart['pieces'] = 0;
		$cart['count'] = 0;
		$cart['total'] = 0;

		if (is_session('cart'))
		{
			foreach ($_SESSION['cart'] as $id => $quantity)
			{
				$cart['products'][$id] = select_product_by_id($database, $id);
				$cart['products'][$id]['image'] = get_image_path($cart['products'][$id]['image']);

				if ($cart['products'][$id]['quantity'] < MAX_QUANTITY)
				{
					$max = $cart['products'][$id]['quantity'];
				}
				else
				{
					$max = MAX_QUANTITY;
				}

				$cart['products'][$id]['quantity'] = $quantity;
				$cart['products'][$id]['total'] = $cart['products'][$id]['cost'] * $quantity;
				$cart['pieces'] += $quantity;
				$cart['count']++;
				$cart['total'] += $cart['products'][$id]['total'];

				$cart['products'][$id]['selectid'] = 'product-' . to_string($id);
				$_POST[$cart['products'][$id]['selectid']] = to_string($quantity);

				for ($index = 0; $index <= $max; $index++)
				{
					$cart['products'][$id]['select'][to_string($index)] = to_string($index);
				}
			}
		}
		else
		{
			$cart['status'] = CART_EMPTY;
		}

		if ($cart['pieces'] >= EXPENSIVE_LIMIT)
		{
			$_SESSION['shipment'] = select_most_expensive_shipment($database);
			$cart['expensive'] = true;
		}
		else
		{
			$cart['expensive'] = false;
		}

		$cart['shipment'] = $cart['shipments'][$_SESSION['shipment']];
		$cart['total'] += $cart['shipment']['cost'];

		if (is_session('discount'))
		{
			$discount = select_discount_by_id($database, $_SESSION['discount']);
			$cart['discount'] = $discount;
			$cart['total'] -= $discount['save'];

			if ($cart['total'] < 0)
			{
				$cart['total'] = 0;
			}
		}
		else
		{
			$cart['discount'] = null;
		}

		$cart['vat_percentage'] = VAT_PERCENTAGE;
		$cart['vat'] = $cart['total'] * VAT_PERCENTAGE / 100;
		$cart['total'] += $cart['vat'];
	}

	$skel = get_skel(
		$database,
		'Carrello',
		'cart',
		'cart',
		array('cart'),
		array('auto-submit', 'modal-focus'));

	require get_page_path('skel');
}

function
get_cart_end_page(object $database)
{
	multi_define_with_prefix('CART_END_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'EMPTY_ERROR',
		'PAYMENT_ERROR',
		'TARGET_INPUT_ERROR',
		'TARGET_DATABASE_ERROR',
		'DATABASE_ERROR'));

	$cart_end['payments'] = select_payments($database);

	if (!is_customer_logged_in())
	{
		$cart_end['status'] = CART_END_LOGIN_ERROR;
	}
	else if (!are_session(array('cart', 'shipment'))) /* discount can be null */
	{
		$cart_end['status'] = CART_END_EMPTY_ERROR;
	}
	else if (!is_post('submit'))
	{
		$cart_end['status'] = CART_END_DEFAULT;
	}
	else if (!is_post_not_empty('payment'))
	{
		$cart_end['status'] = CART_END_PAYMENT_ERROR;
	}
	else
	{
		if (!is_post_not_empty('target'))
		{
			$target = select_target_id_by_customer_id($database, $_SESSION['id']);
		}
		else if (!are_post_not_empty(array('country', 'province', 'city', 'address', 'cap')))
		{
			$cart_end['status'] = CART_END_TARGET_INPUT_ERROR;
		}
		else
		{
			$message = insert_target(
				$database,
				$_POST['country'],
				$_POST['province'],
				$_POST['city'],
				$_POST['address'],
				string_to_int($_POST['cap']));

			if (is_value_not_null($message))
			{
				$cart_end['status'] = CART_END_TARGET_DATABASE_ERROR;
				$cart_end['message'] = $message;
			}
			else
			{
				$target = select_last_target_id($database);
			}
		}

		if (isset($target))
		{
			$message = insert_checkout(
				$database,
				$_SESSION['id'],
				$target,
				$_SESSION['discount'],
				$_SESSION['shipment'],
				string_to_int($_POST['payment']),
				VAT_PERCENTAGE,
				$_SESSION['cart']);

			if (is_value_not_null($message))
			{
				$cart_end['status'] = CART_END_DATABASE_ERROR;
				$cart_end['message'] = $message;
			}
			else
			{
				clear_cart();
				$_SESSION['from'] = 'cart_end';
				redirect_to('category.php');
			}
		}
	}

	$skel = get_skel(
		$database,
		'Completa ordine',
		'cart',
		'cart_end',
		array(),
		array('cart_end'));

	require get_page_path('skel');
}

function
get_notifications_page(object $database)
{
	multi_define_with_prefix('NOTIFICATIONS_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'EMPTY'));

	if (!is_logged_in())
	{
		$notifications['status'] = NOTIFICATIONS_LOGIN_ERROR;
	}
	else
	{
		if (is_post('seen'))
		{
			if (is_customer())
			{
				update_unseen_notifications_by_customer_id($database, $_SESSION['id']);
			}
			else
			{
				update_unseen_notifications_by_seller_id($database, $_SESSION['id']);
			}
		}

		if (is_customer())
		{
			$notifications['list'] = select_notifications_by_customer_id($database, $_SESSION['id']);
		}
		else
		{
			$notifications['list'] = select_notifications_by_seller_id($database, $_SESSION['id']);
		}

		if (is_array_empty($notifications['list']))
		{
			$notifications['status'] = NOTIFICATIONS_EMPTY;
		}
		else
		{
			$notifications['status'] = NOTIFICATIONS_DEFAULT;
		}
	}

	$skel = get_skel_simple($database, 'Notifiche', true, 'notifications');

	require get_page_path('skel');
}

function
get_update_customer_page(object $database)
{
	multi_define_with_prefix('UPDATE_CUSTOMER_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'DATABASE_ERROR'));

	if (!is_customer_logged_in())
	{
		$update_customer['status'] = UPDATE_CUSTOMER_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$update_customer['status'] = UPDATE_CUSTOMER_DEFAULT;

		foreach (select_customer_by_id($database, $_SESSION['id']) as $field => $value)
		{
			if (is_value_null($value))
			{
				$_POST[$field] = '';
			}
			else
			{
				$_POST[$field] = to_string($value);
			}
		}
	}
	else if (!are_post_not_empty(array('name', 'surname', 'email')) || !are_post(array('society', 'phone')))
	{
		$update_customer['status'] = UPDATE_CUSTOMER_INPUT_ERROR;
	}
	else
	{
		$message = update_customer_by_id(
			$database,
			$_SESSION['id'],
			$_POST['name'],
			$_POST['surname'],
			string_to_nullstring($_POST['society']),
			string_to_nullint($_POST['phone']),
			$_POST['email']);

		if (is_value_not_null($message))
		{
			$update_customer['status'] = UPDATE_CUSTOMER_DATABASE_ERROR;
			$update_customer['message'] = $message;
		}
		else
		{
			$update_customer['status'] = UPDATE_CUSTOMER_SUCCESS;
		}
	}

	$skel = get_skel(
		$database,
		'Modifica account',
		'update',
		'update_customer',
		array(),
		array());

	require get_page_path('skel');
}

function
get_update_password_page(object $database)
{
	multi_define_with_prefix('UPDATE_PASSWORD_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'CHECK_ERROR',
		'DATABASE_ERROR'));

	if (!is_customer_logged_in())
	{
		$update_password['status'] = UPDATE_PASSWORD_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$update_password['status'] = UPDATE_PASSWORD_DEFAULT;
	}
	else if (!are_post_not_empty(array('old-password', 'password', 'password-check')))
	{
		$update_password['status'] = UPDATE_PASSWORD_INPUT_ERROR;
	}
	else if ($_POST['password'] !== $_POST['password-check'])
	{
		$update_password['status'] = UPDATE_PASSWORD_CHECK_ERROR;
	}
	else
	{
		$message = update_customer_password_by_id(
			$database,
			$_SESSION['id'],
			$_POST['old-password'],
			$_POST['password']);

		if (is_value_not_null($message))
		{
			$update_password['status'] = UPDATE_PASSWORD_DATABASE_ERROR;
			$update_password['message'] = $message;
		}
		else
		{
			unset($_POST);
			$update_password['status'] = UPDATE_PASSWORD_SUCCESS;
		}
	}

	$skel = get_skel(
		$database,
		'Modifica password',
		'update',
		'update_password',
		array(),
		array('show-password', 'password-check'));

	require get_page_path('skel');
}

function
get_update_target_page(object $database)
{
	multi_define_with_prefix('UPDATE_TARGET_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'DATABASE_ERROR'));

	if (!is_customer_logged_in())
	{
		$update_target['status'] = UPDATE_TARGET_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$update_target['status'] = UPDATE_TARGET_DEFAULT;

		foreach (select_target_by_customer_id($database, $_SESSION['id']) as $field => $value)
		{
			if (is_value_null($value))
			{
				$_POST[$field] = '';
			}
			else
			{
				$_POST[$field] = to_string($value);
			}
		}
	}
	else if (!are_post_not_empty(array('country', 'province', 'city', 'address', 'cap')))
	{
		$update_target['status'] = UPDATE_TARGET_INPUT_ERROR;
	}
	else
	{
		$message = update_target_by_customer_id(
			$database,
			$_SESSION['id'],
			$_POST['country'],
			$_POST['province'],
			$_POST['city'],
			$_POST['address'],
			string_to_int($_POST['cap']));

		if (is_value_not_null($message))
		{
			$update_target['status'] = UPDATE_TARGET_DATABASE_ERROR;
			$update_target['message'] = $message;
		}
		else
		{
			$update_target['status'] = UPDATE_TARGET_SUCCESS;
		}
	}

	$skel = get_skel(
		$database,
		'Modifica destinazione',
		'update',
		'update_target',
		array(),
		array());

	require get_page_path('skel');
}

function
get_insert_category_page(object $database)
{
	multi_define_with_prefix('INSERT_CATEGORY_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'DATABASE_ERROR'));

	if (!is_seller_logged_in())
	{
		$insert_category['status'] = INSERT_CATEGORY_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$insert_category['status'] = INSERT_CATEGORY_DEFAULT;
	}
	else if (!is_post_not_empty('name'))
	{
		$insert_category['status'] = INSERT_CATEGORY_INPUT_ERROR;
	}
	else
	{
		$message = insert_category($database, $_POST['name']);

		if (is_value_not_null($message))
		{
			$insert_category['status'] = INSERT_CATEGORY_DATABASE_ERROR;
			$insert_category['message'] = $message;
		}
		else
		{
			$insert_category['status'] = INSERT_CATEGORY_SUCCESS;
			unset($_POST);
		}
	}

	$skel = get_skel(
		$database,
		'Aggiungi categoria',
		'insert',
		'insert_category',
		array(),
		array());

	require get_page_path('skel');
}

function
get_insert_discount_page(object $database)
{
	multi_define_with_prefix('INSERT_DISCOUNT_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'CODE_ERROR',
		'SAVE_ERROR',
		'DATABASE_ERROR'));

	if (!is_seller_logged_in())
	{
		$insert_discount['status'] = INSERT_DISCOUNT_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$insert_discount['status'] = INSERT_DISCOUNT_DEFAULT;
	}
	else if (!are_post_not_empty(array('code', 'save')))
	{
		$insert_discount['status'] = INSERT_DISCOUNT_INPUT_ERROR;
	}
	else if (strlen($_POST['code']) !== DISCOUNT_LENGTH)
	{
		$insert_discount['status'] = INSERT_DISCOUNT_CODE_ERROR;
	}
	else if (string_to_float($_POST['save']) <= 0)
	{
		$insert_discount['status'] = INSERT_DISCOUNT_SAVE_ERROR;
	}
	else
	{
		$message = insert_discount(
			$database,
			$_POST['code'],
			string_to_float($_POST['save']));

		if (is_value_not_null($message))
		{
			$insert_discount['status'] = INSERT_DISCOUNT_DATABASE_ERROR;
			$insert_discount['message'] = $message;
		}
		else
		{
			$insert_discount['status'] = INSERT_DISCOUNT_SUCCESS;
			unset($_POST);
		}
	}

	$insert_discount['length'] = DISCOUNT_LENGTH;

	$skel = get_skel(
		$database,
		'Aggiungi buono',
		'insert',
		'insert_discount',
		array(),
		array());

	require get_page_path('skel');
}

function
get_insert_product_page(object $database)
{
	multi_define_with_prefix('INSERT_PRODUCT_', array(
		'DEFAULT',
		'SUCCESS',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'FILE_ERROR',
		'DATABASE_ERROR'));

	$insert_product['categories'] = select_categories($database);

	if (!is_seller_logged_in())
	{
		$insert_product['status'] = INSERT_PRODUCT_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$insert_product['status'] = INSERT_PRODUCT_DEFAULT;
	}
	else if (!are_post_not_empty(array('name', 'ingredients', 'weight', 'cost', 'description', 'deadline', 'category', 'quantity')))
	{
		$insert_product['status'] = INSERT_PRODUCT_INPUT_ERROR;
	}
	else if (!isset($_FILES['image']['tmp_name']))
	{
		$insert_product['status'] = INSERT_PRODUCT_FILE_ERROR;
	}
	else
	{
		$file = get_image_path($_FILES['image']['name']);

		if (is_file($file) || !move_uploaded_file($_FILES['image']['tmp_name'], $file) || !chmod($file, 0644))
		{
			$insert_product['status'] = INSERT_PRODUCT_FILE_ERROR;
		}
		else
		{
			$message = insert_product(
				$database,
				$_POST['name'],
				$_POST['ingredients'],
				string_to_int($_POST['weight']),
				string_to_float($_POST['cost']),
				$_POST['description'],
				$_POST['deadline'],
				$_FILES['image']['name'],
				string_to_int($_POST['category']),
				string_to_int($_POST['quantity']));

			if (!is_value_null($message))
			{
				$insert_product['status'] = INSERT_PRODUCT_DATABASE_ERROR;
				$insert_product['message'] = $message;
			}
			else
			{
				$insert_product['status'] = INSERT_PRODUCT_SUCCESS;
				unset($_POST);
			}
		}
	}

	$skel = get_skel(
		$database,
		'Aggiungi prodotto',
		'insert',
		'insert_product',
		array(),
		array('custom-file-input'));

	require get_page_path('skel');
}

function
get_checkouts_page(object $database)
{
	multi_define_with_prefix('CHECKOUTS_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'EMPTY'));

	if (!is_logged_in())
	{
		$checkouts['status'] = CHECKOUTS_LOGIN_ERROR;
	}
	else
	{
		if (is_customer())
		{
			$checkouts['list'] = select_checkouts_by_customer_id($database, $_SESSION['id']);
		}
		else
		{
			$checkouts['list'] = select_checkouts($database);
		}

		if (is_array_empty($checkouts['list']))
		{
			$checkouts['status'] = CHECKOUTS_EMPTY;
		}
		else
		{
			foreach ($checkouts['list'] as $index => $checkout)
			{
				$now = time();

				foreach (array('accepted', 'working', 'sent') as $state)
				{
					$bool = 'is' . $state;

					if (!is_null($checkout[$state]) && $now - strtotime($checkout[$state]) > 0)
					{
						$checkouts['list'][$index][$bool] = true;
					}
					else
					{
						$checkouts['list'][$index][$bool] = false;
					}
				}
			}

			$checkouts['status'] = CHECKOUTS_DEFAULT;
		}
	}

	$skel = get_skel_simple($database, 'Ordini', true, 'checkouts');

	require get_page_path('skel');
}

function
get_logout_page(object $database)
{
	multi_define_with_prefix('LOGOUT_', array(
		'DEFAULT',
		'ALREADY_DONE'));

	if (!is_logged_in())
	{
		$logout['status'] = LOGOUT_ALREADY_DONE;
	}
	else if (is_post('submit'))
	{
		logout();
		$_SESSION['from'] = 'logout';
		redirect_to('login.php');
	}
	else
	{
		$logout['status'] = LOGOUT_DEFAULT;
	}

	$skel = get_skel_simple($database, 'Logout', true, 'logout');

	require get_page_path('skel');
}

function
get_login_page(object $database)
{
	multi_define_with_prefix('LOGIN_', array(
		'DEFAULT',
		'ALREADY_DONE',
		'INPUT_ERROR',
		'FROM_SIGNUP',
		'FROM_LOGOUT'));

	$login['focus'] = 'email';

	if (is_logged_in())
	{
		$login['status'] = LOGIN_ALREADY_DONE;
	}
	else if (is_session_not_empty('from'))
	{
		if ($_SESSION['from'] === 'signup')
		{
			$login['status'] = LOGIN_FROM_SIGNUP;
		}
		else if ($_SESSION['from'] === 'logout')
		{
			$login['status'] = LOGIN_FROM_LOGOUT;
		}

		unset($_SESSION['from']);
	}
	else if (!is_post('submit'))
	{
		$login['status'] = LOGIN_DEFAULT;
	}
	else if (!are_post_not_empty(array('email', 'password')))
	{
		$login['status'] = LOGIN_INPUT_ERROR;
	}
	else
	{
		$customer = is_post('customer');

		if ($customer)
		{
			$id = select_customer_id_by_login(
				$database,
				$_POST['email'],
				$_POST['password']);
		}
		else
		{
			$id = select_seller_id_by_login(
				$database,
				$_POST['email'],
				$_POST['password']);
		}

		if (is_value_null($id))
		{
			$login['status'] = LOGIN_INPUT_ERROR;
			$login['focus'] = 'password';
		}
		else
		{
			login($id, $customer);
			$_SESSION['from'] = 'login';
			redirect_to('category.php');
		}
	}

	$skel = get_skel_simple($database, 'Login', true, 'login');

	require get_page_path('skel');
}

function
get_signup_page(object $database)
{
	multi_define_with_prefix('SIGNUP_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'INPUT_ERROR',
		'PASSWORD_CHECK_ERROR',
		'PRIVACY_POLICY_ERROR',
		'DATABASE_ERROR'));

	if (is_logged_in())
	{
		$signup['status'] = SIGNUP_LOGIN_ERROR;
	}
	else if (!is_post('submit'))
	{
		$signup['status'] = SIGNUP_DEFAULT;
	}
	else if (!are_post_not_empty(array('name', 'surname', 'email', 'password', 'password-check', 'country', 'province', 'city', 'address', 'cap')))
	{
		$signup['status'] = SIGNUP_INPUT_ERROR;
	}
	else if ($_POST['password'] !== $_POST['password-check'])
	{
		$signup['status'] = SIGNUP_PASSWORD_CHECK_ERROR;
	}
	else if (!is_post('privacy-policy'))
	{
		$signup['status'] = SIGNUP_PRIVACY_POLICY_ERROR;
	}
	else
	{
		$message = insert_customer(
			$database,
			$_POST['name'],
			$_POST['surname'],
			string_to_nullstring($_POST['society']),
			string_to_nullint($_POST['phone']),
			$_POST['email'],
			$_POST['password'],
			$_POST['country'],
			$_POST['province'],
			$_POST['city'],
			$_POST['address'],
			string_to_int($_POST['cap']));

		if (is_value_not_null($message))
		{
			$signup['status'] = SIGNUP_DATABASE_ERROR;
			$signup['message'] = $message;
		}
		else
		{
			$_SESSION['from'] = 'signup';
			redirect_to('login.php');
		}
	}

	$skel = get_skel(
		$database,
		'Signup',
		'signup',
		'signup',
		array(),
		array('show-password', 'password-check'));

	require get_page_path('skel');
}

function
get_contacts_page(object $database)
{
	get_page($database, 'Contatti', true, 'contacts');
}

function
get_privacy_page(object $database)
{
	get_page($database, 'Privacy', true, 'privacy');
}

function
get_search_page(object $database)
{
	multi_define_with_prefix('SEARCH_', array(
		'DEFAULT',
		'EMPTY'));

	if (is_get('name'))
	{
		$name = $_GET['name'];

		$search['limit'] = string_to_int(default_get('limit', PAGE_LIMIT));
		$search['offset'] = string_to_int(default_get('offset', 0));

		$search['products'] = select_products_by_name_match(
			$database,
			$name,
			$search['limit'],
			$search['offset']);

		if (is_array_empty($search['products']))
		{
			$search['status'] = SEARCH_EMPTY;
		}
		else
		{
			$search['status'] = SEARCH_DEFAULT;
			$search['prefix'] = 'search.php?name=' . $name;
			$search['count'] = count_products_by_name_match($database, $name);
		}
	}
	else
	{
		$name = '';
		$search['status'] = SEARCH_EMPTY;
	}

	$skel = get_skel(
		$database,
		'Cerca: ' . $name,
		null,
		'search',
		array(),
		array());

	require get_page_path('skel');
}

function
get_product_page(object $database)
{
	multi_define_with_prefix('PRODUCT_', array(
		'DEFAULT',
		'NOT_FOUND',
		'INVALID_QUANTITY',
		'ADDED',
		'UPDATED',
		'QUANTITY_UPDATED',
		'DATABASE_ERROR'));

	$redirect = false;

	if (is_get('id'))
	{
		$id = string_to_nullint($_GET['id']);

		if (is_value_not_null($id))
		{
			$product = select_product_by_id($database, $id);
		}
		else
		{
			$product = array();
		}
	}
	else
	{
		$product = array();
	}

	if (is_array_empty($product))
	{
		$product['status'] = PRODUCT_NOT_FOUND;
		$product['name'] = 'Non trovato';
	}
	else
	{
		$product['customer'] = is_customer_logged_in();
		$product['seller'] = is_seller_logged_in();
		$product['image'] = get_image_path($product['image']);

		if ($product['customer'])
		{
			$product['min'] = 1;

			if ($product['quantity'] < MAX_QUANTITY)
			{
				$product['max'] = $product['quantity'];
			}
			else
			{
				$product['max'] = MAX_QUANTITY;
			}

			$product['cart'] = isset($_SESSION['cart'][$id]);

			if (is_post('submit') && is_post_not_empty('quantity') && $product['quantity'] > 0)
			{
				$quantity = string_to_nullint($_POST['quantity']);

				if (is_value_null($quantity) || $quantity < $product['min'] || $quantity > $product['max'])
				{
					$product['status'] = PRODUCT_INVALID_QUANTITY;
				}
				else
				{
					$_SESSION['cart'][$id] = $quantity;

					if ($product['cart'])
					{
						$product['status'] = PRODUCT_UPDATED;
					}
					else
					{
						$product['status'] = PRODUCT_ADDED;
						$product['cart'] = true;
					}

					if ($_POST['submit'] === 'cart')
					{
						$redirect = true;
					}
				}
			}
			else
			{
				if ($product['cart'])
				{
					$_POST['quantity'] = to_string($_SESSION['cart'][$id]);
				}

				$product['status'] = PRODUCT_DEFAULT;
			}
		}
		else if ($product['seller'])
		{
			if (is_get('seen'))
			{
				update_seller_notification_by_id($database, string_to_int($_GET['seen']), $_SESSION['id']);
			}

			if (is_post('submit') && is_post_not_empty('quantity') && $_POST['quantity'] >= 0)
			{
				$message = update_product_quantity_by_id($database, $id, string_to_int($_POST['quantity']));

				if (is_value_not_null($message))
				{
					$product['status'] = PRODUCT_DATABASE_ERROR;
					$product['message'] = $message;
				}
				else
				{
					$product['status'] = PRODUCT_QUANTITY_UPDATED;
					$product['quantity'] = $_POST['quantity'];
				}
			}
			else
			{
				$product['status'] = PRODUCT_DEFAULT;
			}
		}
		else
		{
			$product['status'] = PRODUCT_DEFAULT;
		}
	}

	if ($redirect)
	{
		if ($product['status'] === PRODUCT_ADDED)
		{
			$_SESSION['from'] = 'added_product';
		}
		else
		{
			$_SESSION['from'] = 'updated_product';
		}

		redirect_to('cart.php');
	}
	else
	{
		$skel = get_skel_simple($database, $product['name'], false, 'product');

		require get_page_path('skel');
	}
}

function
get_checkout_page(object $database)
{
	multi_define_with_prefix('CHECKOUT_', array(
		'DEFAULT',
		'LOGIN_ERROR',
		'NOT_FOUND',
		'WORKING_SET',
		'SENT_SET',
		'DATABASE_ERROR'));

	if (!is_logged_in())
	{
		$checkout['status'] = CHECKOUT_LOGIN_ERROR;
	}
	else
	{
		if (is_get('seen'))
		{
			update_customer_notification_by_id(
				$database,
				string_to_int($_GET['seen']),
				$_SESSION['id']);
		}

		if (is_get('id'))
		{
			$id = string_to_nullint($_GET['id']);

			if (is_value_not_null($id))
			{
				if (is_post('working'))
				{
					$message = update_checkout_state_by_id(
						$database,
						$id,
						CHECKOUT_STATE_WORKING);

					if (is_value_not_null($message))
					{
						$status = CHECKOUT_DATABASE_ERROR;
					}
					else
					{
						$status = CHECKOUT_WORKING_SET;
					}
				}
				else if (is_post('sent'))
				{
					$message = update_checkout_state_by_id(
						$database,
						$id,
						CHECKOUT_STATE_SENT);

					if (is_value_not_null($message))
					{
						$status = CHECKOUT_DATABASE_ERROR;
					}
					else
					{
						$status = CHECKOUT_SENT_SET;
					}
				}
				else
				{
					$message = '';
					$status = CHECKOUT_DEFAULT;
				}

				$checkout = select_checkout_by_id(
					$database,
					$id,
					$_SESSION['id']);
			}
			else
			{
				$checkout['partial'] = null;
			}
		}
		else
		{
			$checkout['partial'] = null;
		}

		/* if (is_array_empty($checkout)) */
		if (is_value_null($checkout['partial']))
		{
			$checkout['status'] = CHECKOUT_NOT_FOUND;
		}
		else
		{
			$checkout['status'] = $status;
			$checkout['message'] = $message;
			$checkout['products'] = select_checkout_products_by_id($database, $id);

			foreach ($checkout['products'] as $product_id => $product)
			{
				$checkout['products'][$product_id]['total'] = $product['cost'] * $product['quantity'];
			}

			$checkout['vat_percentage'] = $checkout['vat'];
			$checkout['vat'] = $checkout['partial'] * $checkout['vat_percentage'] / 100;
			$checkout['total'] = $checkout['partial'] + $checkout['vat'];
		}

		$checkout['button']['working'] = false;
		$checkout['button']['sent'] = false;

		if (is_seller())
		{
			if (is_value_null($checkout['working']))
			{
				$checkout['button']['working'] = true;
			}
			else if (is_value_null($checkout['sent']))
			{
				$checkout['button']['sent'] =  true;
			}
		}
	}

	$skel = get_skel_simple($database, 'Ordine ' . $id, false, 'checkout');

	require get_page_path('skel');
}
