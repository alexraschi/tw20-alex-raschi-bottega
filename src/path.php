<?php
declare(strict_types = 1);

function
get_image_path(string $image): string
{
	return IMAGES_DIR . $image;
}

function
get_page_path(string $page): string
{
	return PAGES_DIR . $page . '.php';
}

function
get_style_path(string $style): string
{
	return STYLES_DIR . $style . '.css';
}

function
get_script_path(string $script): string
{
	return SCRIPTS_DIR . $script . '.js';
}
