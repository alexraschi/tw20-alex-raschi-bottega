<?php
declare(strict_types = 1);
session_start();
require_once 'src/database.php';
require_once 'src/form.php';
require_once 'src/message.php';
require_once 'src/page.php';
require_once 'src/path.php';
require_once 'src/skel.php';
require_once 'src/tool.php';

const IMAGES_DIR = 'img/';
const PAGES_DIR = 'page/';
const STYLES_DIR = 'css/';
const SCRIPTS_DIR = 'script/';
const PAGE_LIMIT = 10;
const MAX_QUANTITY = 16;
const DISCOUNT_LENGTH = 8;
const VAT_PERCENTAGE = 22;
const EXPENSIVE_LIMIT = 40;

/* note: a salt will be generated automatically by password_hash(), argon2 is
   not supported by xampp, anyway bcrypt is a strong algorithm */
const PASSWORD_HASH = PASSWORD_BCRYPT;

$database = database_connect('localhost', 'root', '', 'shop', 'it_IT');
multi_define_with_prefix('CHECKOUT_STATE_', array('ACCEPTED', 'WORKING', 'SENT'));
