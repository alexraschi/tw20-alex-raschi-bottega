<?php
declare(strict_types = 1);

if ($notifications['status'] === NOTIFICATIONS_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login prima di poter utilizzare questa pagina.');
}
else if ($notifications['status'] === NOTIFICATIONS_EMPTY)
{
	get_error_message('Non sono presenti notifiche al momento.');
}
else
{
?>
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
		</div>
		<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
			<form method="post" class="d-flex justify-content-end mb-2">
				<button name="seen" type="submit" class="btn btn-link text-decoration-none">Imposta tutti come già letti</button>
			</form>
			<ul class="shadow-sm list-group mb-3">
				<?php
				foreach ($notifications['list'] as $notification)
				{
				?>
					<li class="list-group-item">
				<?php
					if ($notification['seen'] === 1)
					{
						$text = 'secondary';
					}
					else
					{
						$text = 'primary';
					}
					if (isset($notification['checkout']))
					{
						if ($notification['type'] === 'checkout_accepted')
						{
							$message = 'L\'ordine ' . $notification['checkout'] . ' è stato accettato';
						}
						else if ($notification['type'] === 'checkout_working')
						{
							$message = 'L\'ordine ' . $notification['checkout'] . ' è in lavorazione';
						}
						else if ($notification['type'] === 'checkout_sent')
						{
							$message = 'Gli oggetti dell\'ordine ' . $notification['checkout'] . ' sono stati inviati';
						}
				?>
						<a class="text-decoration-none text-<?= $text ?>" href="checkout.php?id=<?= $notification['checkout'] ?>&seen=<?= $notification['id'] ?>"><?= $message ?>.</a>
				<?php
					}
					else if (isset($notification['product']))
					{
						if ($notification['type'] === 'product_outofstock')
						{
							$message = 'Il prodotto "' . $notification['product_name'] . '" si è esaurito';
						}
				?>
						<a class="text-decoration-none text-<?= $text ?>" href="product.php?id=<?= $notification['product'] ?>&seen=<?= $notification['id'] ?>"><?= $message ?>.</a>
				<?php
					}
				?>
					</li>
				<?php
				}
				?>
			</ul>
		</div>
	</div>
<?php
}
?>
