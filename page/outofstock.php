<?php
declare(strict_types = 1);

if ($outofstock['status'] === OUTOFSTOCK_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come venditore prima di poter utilizzare questa pagina.');
}
else if ($outofstock['status'] === OUTOFSTOCK_EMPTY)
{
	get_error_message('Categoria (temporaneamente) vuota, torna indietro o selezionane un\'altra.');
}
else
{
	get_list($outofstock['products'], $outofstock['prefix'], $outofstock['limit'], $outofstock['offset'], $outofstock['count']);
}
