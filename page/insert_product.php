<?php
declare(strict_types = 1);

if ($insert_product['status'] === INSERT_PRODUCT_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come venditore prima di poter utilizzare questa pagina.');
}
else
{
	if ($insert_product['status'] === INSERT_PRODUCT_SUCCESS)
	{
		get_success_message('Hai inserito il nuovo prodotto con successo!');
	}
	else if ($insert_product['status'] === INSERT_PRODUCT_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni per il nuovo prodotto in modo parziale.');
	}
	else if ($insert_product['status'] === INSERT_PRODUCT_FILE_ERROR)
	{
		get_error_message('Errore nel caricamento del file.');
	}
	else if ($insert_product['status'] === INSERT_PRODUCT_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni per il nuovo prodotto in modo errato. Messaggio di errore: ' . $insert_product['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-sm-10 col-lg-8 col-xl-6">
			<form method="post" enctype="multipart/form-data">
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php get_input_form('name', false, 'Nome', 'text', 'Formaggio buono', 'required'); ?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php get_input_form('weight', false, 'Peso', 'number', '100', 'min="0" required'); ?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php get_input_form('cost', false, 'Costo', 'number', '5', 'min="0" step="0.01" required'); ?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php get_input_form('deadline', false, 'Scadenza', 'date', '', 'required'); ?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php
								foreach ($insert_product['categories'] as $category)
								{
									$options[$category['id']] = $category['name'];
								}
								get_select_form('category', false, 'Categoria', '', true, $options, 'required');
								?>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12 mb-2">
								<?php get_input_form('quantity', false, 'Quantità disponibile', 'number', '32', 'min="0" required'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mb-2">
							<?php get_textarea_form('ingredients', 'Ingredienti', 'acqua, latte.', 'required'); ?>
						</div>
						<div class="mb-2">
							<?php get_textarea_form('description', 'Descrizione', 'Formaggio prodotto qua vicino.', 'required'); ?>
						</div>
						<div class="mt-3">
							<?php get_input_form('image', false, 'Immagine', 'file', '', 'required'); ?>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 my-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiungi prodotto</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
