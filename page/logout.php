<?php
declare(strict_types = 1);

if ($logout['status'] === LOGOUT_ALREADY_DONE)
{
	get_error_message('Hai già effettuato il logout.');
}
else
{
?>
	<div class="row">
		<div class="col-sm-2 col-md-3 col-xl-4">
		</div>
		<div class="col-sm-8 col-md-6 col-xl-4">
			<form method="post">
				<button class="btn btn-lg btn-danger btn-block mb-3" type="submit" name="submit">Logout</button>
			</form>
		</div>
	</div>
<?php
}
?>
