<?php
declare(strict_types = 1);

if ($insert_discount['status'] === INSERT_DISCOUNT_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come venditore prima di poter utilizzare questa pagina.');
}
else
{
	if ($insert_discount['status'] === INSERT_DISCOUNT_SUCCESS)
	{
		get_success_message('Hai inserito il nuovo buono con successo!');
	}
	else if ($insert_discount['status'] === INSERT_DISCOUNT_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni per il nuovo buono in modo parziale.');
	}
	else if ($insert_discount['status'] === INSERT_DISCOUNT_CODE_ERROR)
	{
		get_error_message('Hai inserito un codice non valido.');
	}
	else if ($insert_discount['status'] === INSERT_DISCOUNT_SAVE_ERROR)
	{
		get_error_message('Hai inserito un risparmio non valido.');
	}
	else if ($insert_discount['status'] === INSERT_DISCOUNT_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni per il nuovo buono in modo errato. Messaggio di errore: ' . $insert_discount['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
		</div>
		<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
			<form method="post">
				<div class="form-row">
					<div class="col-sm-6 mb-2">
						<?php get_input_form('code', false, 'Codice', 'text', '19485766', 'minlength="' . $insert_discount['length'] . '" maxlength="' . $insert_discount['length'] . '" required'); ?>
					</div>
					<div class="col-sm-6 mb-2">
						<?php get_input_form('save', false, 'Risparmio', 'number', '20', 'min="0.01" step="0.01" required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 my-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiungi buono</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
