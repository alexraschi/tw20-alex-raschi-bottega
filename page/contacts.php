<?php
declare(strict_types = 1);
?>
<div class="row">
	<div class="col-sm-1 col-md-2 col-lg-3">
	</div>
	<div class="col-sm-10 col-md-8 col-lg-6 mb-3">
		<h2 class="my-3">Assistenza</h2>
		<p>I nostri operatori sono a tua disposizione per assisterti. L'assistenza è gratuita, possiamo aiutarti?</p>
		<p><a href="tel:+390549485213">+39 0549 485 213</a><br />dal Lunedì al Venerdì ore 08-16, Sabato ore 08-12</p>
		<h2 class="my-3">Recapiti</h2>
		<p>Gianpiero Productions Srl</p>
		<p>Via Marecchiese 1657<br />Rimini (RN) Italia</p>
		<p>CIAA e P.iva e CF 36746569322<br />REA 349565<br />Capitale Sociale &euro; 10.000 I.V.<br />Licenza SIAE 1859/I/1244 </p>
	</div>
</div>
