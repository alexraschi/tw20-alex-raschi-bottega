<?php
declare(strict_types = 1);

if ($update_target['status'] === UPDATE_TARGET_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come cliente prima di poter utilizzare questa pagina.');
}
else
{
	if ($update_target['status'] === UPDATE_TARGET_SUCCESS)
	{
		get_success_message('Hai modificato la destinazione con successo!');
	}
	else if ($update_target['status'] === UPDATE_TARGET_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo parziale.');
	}
	else if ($update_target['status'] === UPDATE_TARGET_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo errato. Messaggio di errore: ' . $update_target['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-sm-10 col-lg-8 col-xl-6">
			<form method="post">
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('country',  false, 'Paese', 'text', '',  'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('province', false, 'Provincia', 'text', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('city', false, 'Città', 'text', '', 'required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('address', false, 'Indirizzo (Via e Numero)', 'text', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('cap', false, 'CAP', 'number', '', 'min="0" required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 my-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiorna destinazione</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
