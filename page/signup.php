<?php
declare(strict_types = 1);

if ($signup['status'] === SIGNUP_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il logout prima di poter utilizzare questa pagina.');
}
else
{
	if ($signup['status'] === SIGNUP_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la registrazione in modo parziale.');
	}
	else if ($signup['status'] === SIGNUP_PASSWORD_CHECK_ERROR)
	{
		get_error_message('La due password che hai immesso non coincidono.');
	}
	else if ($signup['status'] === SIGNUP_PRIVACY_POLICY_ERROR)
	{
		get_error_message('Devi accettare la privacy policy.');
	}
	else if ($signup['status'] === SIGNUP_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la registrazione in modo errato. Messaggio di errore: ' . $signup['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-lg-2">
		</div>
		<div class="col-sm-10 col-lg-8">
			<form method="post">
				<h2 class="h3 mb-3">Account</h2>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('name', false, 'Nome', 'text', 'Luca', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('surname', false, 'Cognome', 'text', 'Pierluigi', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('society', false, 'Società <span class="text-muted">(Opzionale)</span>', 'text', 'Super spedizioni 3000', ''); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('phone', false, 'Telefono <span class="text-muted">(Opzionale)</span>', 'number', '3344583945', ''); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('email', false, 'Email', 'email', 'luca@example.com', 'required'); ?>
					</div>
					<div class="col-md-4">
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('password', false, 'Password', 'password', 'qualitypassword1980', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('password-check', false, 'Ripeti password', 'password', 'qualitypassword1980', 'required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4">
						<?php get_checkbox_form('show-password', 'Mostra password', '', true, ''); ?>
					</div>
					<div class="col-md-8">
					</div>
				</div>
				<hr class="mb-3" />
				<h2 class="h3 mb-3">Destinazione</h2>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<?php get_input_form('country',  false, 'Paese', 'text', 'Rimini',  'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('province', false, 'Provincia', 'text', 'Rimini', 'required'); ?>
					</div>
					<div class="col-md-3 mb-3">
						<?php get_input_form('city', false, 'Città', 'text', 'Rimini', 'required'); ?>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('address', false, 'Indirizzo (Via e Numero)', 'text', 'Via di Mezzo 2', 'required'); ?>
					</div>
					<div class="col-md-3">
						<?php get_input_form('cap', false, 'CAP', 'number', '23419', 'min="0" required'); ?>
					</div>
					<div class="col-md-5">
					</div>
				</div>
				<hr class="mb-3" />
				<div class="form-row">
					<div class="col mb-3">
						<?php get_checkbox_form('privacy-policy', 'Accetto la <a href="privacy.php">Privacy Policy</a>', 'required', false); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-1 col-lg-3 col-xl-4">
					</div>
					<div class="col-md-8 col-lg-6 col-xl-4 mb-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Registrati</button>
					</div>
					<div class="col-md-1 col-lg-3 col-xl-4">
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
