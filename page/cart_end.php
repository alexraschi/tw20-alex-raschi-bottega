<?php
declare(strict_types = 1);

if ($cart_end['status'] === CART_END_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come cliente prima di poter utilizzare questa pagina.');
}
else if ($cart_end['status'] === CART_END_EMPTY_ERROR)
{
	get_error_message('Devi prima aggiungere almeno un prodotto al carrello prima di poter utilizzare questa pagina.');
}
else
{
	if ($cart_end['status'] === CART_END_PAYMENT_ERROR)
	{
		get_error_message('Non hai impostato il metodo di pagamento.');
	}
	else if ($cart_end['status'] === CART_END_TARGET_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la destinazione in modo parziale.');
	}
	else if ($cart_end['status'] === CART_END_TARGET_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la destinazione in modo errato. Messaggio di errore: ' . $cart_end['message']);
	}
	else if ($cart_end['status'] === CART_END_DATABASE_ERROR)
	{
		get_error_message('Si è verificato un errore nell\'inserimento delle informazioni. Messaggio di errore: ' . $cart_end['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-md-2 col-xl-3">
		</div>
		<div class="col-sm-10 col-md-8 col-xl-6">
			<form method="post">
				<h2 class="h3 mb-3">Spedizione</h2>
				<div class="mb-3">
					<?php get_checkbox_form('target', 'Utilizza una destinazione alternativa', '', false, ''); ?>
				</div>
				<div class="target">
					<div class="row">
						<div class="col-md-4 mb-3">
							<?php get_input_form('country', false, 'Paese', 'text', 'Italia', ''); ?>
						</div>
						<div class="col-md-4 mb-3">
							<?php get_input_form('province', false, 'Provincia', 'text', 'Rimini', ''); ?>
						</div>
						<div class="col-md-4 mb-3">
							<?php get_input_form('city', false, 'Città', 'text', 'Rimini', ''); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 mb-3">
							<?php get_input_form('address', false, 'Indirizzo (Via e Numero)', 'text', 'Via di Mezzo 2', ''); ?>
						</div>
						<div class="col-md-4 mb-3">
							<?php get_input_form('cap', false, 'CAP', 'number', '23419', 'min="0"'); ?>
						</div>
					</div>
				</div>
				<hr class="mb-3" />
				<h2 class="h3 mb-3">Pagamento</h2>
				<div class="my-3">
					<?php
					$checked = null;
					foreach ($cart_end['payments'] as $payment)
					{
						if (is_value_null($checked))
						{
							$checked = $payment['id'];
						}
						$labels[$payment['id']] = $payment['name'];
					}
					get_radio_form('payment', 'Pagamento', $labels, $checked, '', 'required');
					?>
				</div>
				<hr class="mb-3" />
				<button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">Completa l'ordine</button>
			</form>
		</div>
	</div>
<?php
}
?>
