<?php
declare(strict_types = 1);
?>
<!doctype html>
<html lang="it">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="stylesheet" href="lib/bootstrap-4.5.3.css" />
		<script src="lib/jquery-3.5.1.js"></script>
		<script src="lib/popper-1.16.1.js"></script>
		<script src="lib/bootstrap-4.5.3.js"></script>
		<script src="lib/bs-custom-file-input-1.3.4.js"></script>
		<?php
		foreach ($skel['styles'] as $style)
		{
		?>
			<link rel="stylesheet" href="<?= $style ?>" />
		<?php
		}
		foreach ($skel['scripts'] as $script)
		{
		?>
			<script src="<?= $script ?>"></script>
		<?php
		}
		?>
		<title><?= $skel['title'] ?></title>
	</head>
	<body class="bg-white">
		<div class="container-fluid px-0 d-flex flex-column min-vh-100">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Espandi barra di navigazione">
					<span class="navbar-toggler-icon">
					</span>
				</button>
				<a class="navbar-brand" href="index.php">Bottega di Gianpiero</a>
				<div class="collapse navbar-collapse" id="navbar">
					<ul class="navbar-nav mr-3">
						<?php
						get_navlink('Home', 'index.php', is_nav($skel, 'home'));
						?>
						<li class="nav-item dropdown<?php if (is_nav($skel, 'categories')) { echo ' active'; } ?>">
							<a class="nav-link dropdown-toggle" href="" id="categories" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorie</a>
							<div class="dropdown-menu" aria-labelledby="categories">
									<a class="dropdown-item" href="category.php">Tutti</a>
								<?php
								if ($skel['seller'])
								{
								?>
									<a class="dropdown-item" href="outofstock.php">Esauriti</a>
								<?php
								}
								foreach ($skel['categories'] as $skel_category)
								{
								?>
									<a class="dropdown-item" href="category.php?id=<?= $skel_category['id'] ?>"><?= $skel_category['name'] ?></a>
								<?php
								}
								?>
							</div>
						</li>
						<?php
						if ($skel['loggedin'])
						{
							if (is_value_null($skel['notifications']))
							{
								$badge = '';
							}
							else
							{
								$badge = ' <span class="badge badge-pill badge-primary">' . $skel['notifications'] . '</span>';
							}
							get_navlink('Notifiche' . $badge,	'notifications.php',	is_nav($skel, 'notifications'));

							if ($skel['customer'])
							{
								if (is_value_null($skel['cart']))
								{
									$badge = '';
								}
								else
								{
									$badge = ' <span class="badge badge-pill badge-success">' . $skel['cart'] . '</span>';
								}
								get_navlink('Carrello' . $badge, 'cart.php', is_nav($skel, 'cart'));
						?>
								<li class="nav-item dropdown<?php if (is_nav($skel, 'update')) { echo ' active'; } ?>">
									<a class="nav-link dropdown-toggle" href="" id="update" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Modifica</a>
									<div class="dropdown-menu" aria-labelledby="update">
										<a class="dropdown-item" href="update_customer.php">Account</a>
										<a class="dropdown-item" href="update_password.php">Password</a>
										<a class="dropdown-item" href="update_target.php">Destinazione</a>
									</div>
								</li>
						<?php
							}
							else
							{
						?>
								<li class="nav-item dropdown<?php if (is_nav($skel, 'insert')) { echo ' active'; } ?>">
									<a class="nav-link dropdown-toggle" href="" id="insert" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aggiungi</a>
									<div class="dropdown-menu" aria-labelledby="insert">
										<a class="dropdown-item" href="insert_category.php">Categoria</a>
										<a class="dropdown-item" href="insert_discount.php">Buono</a>
										<a class="dropdown-item" href="insert_product.php">Prodotto</a>
									</div>
								</li>
						<?php
							}
							get_navlink('Ordini',	'checkouts.php',	is_nav($skel, 'checkouts'));
							get_navlink('Logout',	'logout.php',		is_nav($skel, 'logout'));
						}
						else
						{
							get_navlink('Login',	'login.php',	is_nav($skel, 'login'));
							get_navlink('Signup',	'signup.php',	is_nav($skel, 'signup'));
							get_navlink('Contatti',	'contacts.php',	is_nav($skel, 'contacts'));
							get_navlink('Privacy',	'privacy.php',	is_nav($skel, 'privacy'));
						}
						?>
					</ul>
					<form class="input-group my-2 my-lg-0" action="search.php">
						<input class="form-control" type="text" id="search" name="name" placeholder="Riso" aria-label="Search" />
						<label for="search" class="sr-only">Cerca</label>
						<div class="input-group-append">
							<button class="btn btn-success" type="submit"><span class="mx-2">Cerca</span></button>
						</div>
					</form>
				</div>
			</nav>
			<main class="flex-fill mb-3">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
						</div>
						<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
							<h1 class="text-center mb-3"><?= $skel['title'] ?></h1>
						</div>
					</div>
					<?php require($skel['main']); ?>
				</div>
			</main>
			<footer class="footer mt-auto py-3 bg-light">
				<div class="container-fluid">
					<div class="text-dark text-center">
						<p class="my-0">Gianpiero Productions Srl<a class="my-0 ml-3 text-decoration-none" href="contacts.php">Contatti</a><a class="my-0 ml-3 text-decoration-none" href="privacy.php">Privacy</a></p>
					</div>
				</div>
			</footer>
		</div>
	</body>
</html>
