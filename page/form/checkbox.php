<?php
declare(strict_types = 1);
?>
<div class="custom-control custom-checkbox">
	<input type="checkbox" class="custom-control-input" id="<?= $checkbox['id'] ?>" <?= $checkbox['attr'] ?>/>
	<label class="custom-control-label" for="<?= $checkbox['id'] ?>"><?= $checkbox['label'] ?></label>
</div>
