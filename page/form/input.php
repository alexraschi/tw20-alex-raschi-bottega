<?php
declare(strict_types = 1);

if (	$input['type'] === 'email'	||
	$input['type'] === 'number'	||
	$input['type'] === 'password'	||
	$input['type'] === 'search'	||
	$input['type'] === 'tel'	||
	$input['type'] === 'text'	||
	$input['type'] === 'url')
{
	$input['attr'] = 'placeholder="' . $input['placeholder'] . '" ' . $input['attr'];
}
?>
<label for="<?= $input['id'] ?>"<?php if ($input['hidelabel']) { echo ' class="sr-only"'; } ?>><?= $input['label'] ?></label>
<input type="<?= $input['type'] ?>" class="form-control" id="<?= $input['id'] ?>" name="<?= $input['id'] ?>" value="<?= $input['value'] ?>" <?= $input['attr'] ?>/>
