<?php
declare(strict_types = 1);
?>
<fieldset>
	<legend class="sr-only"><?= $radio['legend'] ?></legend>
	<?php
	foreach ($radio['options'] as $id => $label)
	{
		if ($id === $radio['checked'])
		{
			$checked = 'checked ';
		}
		else
		{
			$checked = '';
		}
	?>
		<div class="custom-control custom-radio">
			<input type="radio" id="<?= $radio['id'] . $id ?>" name="<?= $radio['id'] ?>" value="<?= $id ?>" class="custom-control-input<?= $radio['class'] ?>" <?= $checked ?><?= $radio['attr'] ?>/>
			<label class="custom-control-label" for="<?= $radio['id'] . $id ?>"><?= $label ?></label>
		</div>
	<?php
	}
	?>
</fieldset>
