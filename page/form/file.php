<?php
declare(strict_types = 1);
?>
<div class="custom-file">
	<label for="<?= $input['id'] ?>" class="custom-file-label"><?= $input['label'] ?></label>
	<input type="file" class="custom-file-input" id="<?= $input['id'] ?>" name="<?= $input['id'] ?>" <?= $input['attr'] ?>/>
</div>
