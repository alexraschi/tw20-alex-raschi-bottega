<?php
declare(strict_types = 1);
?>
<label for="<?= $textarea['id'] ?>"><?= $textarea['label'] ?></label>
<textarea class="form-control" id="<?= $textarea['id'] ?>" name="<?= $textarea['id'] ?>" placeholder="<?= $textarea['placeholder'] ?>"<?= $textarea['attr'] ?>><?= $textarea['value'] ?></textarea>
