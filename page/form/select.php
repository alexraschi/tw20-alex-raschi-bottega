<?php
declare(strict_types = 1);
?>
<label for="<?= $select['id'] ?>"<?php if ($select['hidelabel']) { echo ' class="sr-only"'; } ?>><?= $select['label'] ?></label>
<select class="custom-select<?= $select['class'] ?>" id="<?= $select['id'] ?>" name="<?= $select['id'] ?>"<?= $select['attr'] ?>>
	<?php
	if ($select['empty'])
	{
	?>
		<option value="">Scegli</option>
	<?php
	}
	foreach ($select['options'] as $id => $name)
	{
		if (is_string_not_empty($select['value']) && string_to_int($select['value']) === $id)
		{
			$selected = ' selected';
		}
		else
		{
			$selected = '';
		}
	?>
		<option value="<?= $id ?>"<?= $selected ?>><?= $name ?></option>
	<?php
	}
	?>
</select>
