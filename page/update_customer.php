<?php
declare(strict_types = 1);

if ($update_customer['status'] === UPDATE_CUSTOMER_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come cliente prima di poter utilizzare questa pagina.');
}
else
{
	if ($update_customer['status'] === UPDATE_CUSTOMER_SUCCESS)
	{
		get_success_message('Hai modificato le informazioni relative al tuo account con successo!');
	}
	else if ($update_customer['status'] === UPDATE_CUSTOMER_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo parziale.');
	}
	else if ($update_customer['status'] === UPDATE_CUSTOMER_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo errato. Messaggio di errore: ' . $update_customer['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-sm-10 col-lg-8 col-xl-6">
			<form method="post">
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('name', false, 'Nome', 'text', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('surname', false, 'Cognome', 'text', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('society', false, 'Società <span class="text-muted">(Opzionale)</span>', 'text', '', ''); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('phone', false, 'Telefono <span class="text-muted">(Opzionale)</span>', 'tel', '', ''); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('email', false, 'Email', 'email', '', 'required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 my-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiorna account</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
