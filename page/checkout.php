<?php
declare(strict_types = 1);

if ($checkout['status'] === CHECKOUT_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login prima di poter utilizzare questa pagina.');
}
else if ($checkout['status'] === CHECKOUT_NOT_FOUND)
{
	get_error_message('Ordine non trovato.');
}
else
{
	if ($checkout['status'] === CHECKOUT_WORKING_SET)
	{
		get_success_message('Hai impostato l\'ordine come in lavorazione!');
	}
	else if ($checkout['status'] === CHECKOUT_SENT_SET)
	{
		get_success_message('Hai impostato l\'ordine come inviato!');
	}
	else if ($checkout['status'] === CHECKOUT_DATABASE_ERROR)
	{
		get_error_message('Si è verificato un errore nell\'aggiornamento del database. Messaggio di errore: ' . $checkout['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-lg-1 col-xl-2">
				</div>
				<div class="col-md-6 col-lg-5 col-xl-4">
					<div class="card mb-3 shadow-sm">
						<div class="card-header">
							<h2 class="h1 mb-0 text-center h3">Ordine</h2>
						</div>
						<div class="card-body">
							<p>Accettato il: <?= $checkout['accepted'] ?></p>
							<?php
							if (is_value_not_null($checkout['working']))
							{
							?>
								<p>In lavorazione dal: <?= $checkout['working'] ?></p>
							<?php
							}
							if (is_value_not_null($checkout['sent']))
							{
							?>
								<p>Spedito il: <?= $checkout['sent'] ?></p>
							<?php
							}
							?>
							<p class="mb-0">Pagamento con: <?= $checkout['payment'] ?></p>
						</div>
					</div>
					<div class="card mb-3 shadow-sm">
						<div class="card-header">
							<h2 class="h1 mb-0 text-center h3">Spedizione</h2>
						</div>
						<div class="card-body">
							<p>Stato: <?= $checkout['country'] ?></p>
							<p>Provincia: <?= $checkout['province'] ?></p>
							<p>Città: <?= $checkout['city'] ?></p>
							<p>Indirizzo: <?= $checkout['address'] ?></p>
							<p class="mb-0">CAP: <?= $checkout['cap'] ?></p>
						</div>
					</div>
					<?php
					if ($checkout['button']['working'])
					{
						$enable = true;
						$id = 'working';
						$label = 'Imposta come in lavorazione';
					}
					else if ($checkout['button']['sent'])
					{
						$enable = true;
						$id = 'sent';
						$label = 'Imposta come inviato';
					}
					else
					{
						$enable = false;
					}

					if ($enable)
					{
					?>
						<form method="post">
							<button name="<?= $id ?>" type="submit" class="btn btn-lg btn-block btn-primary"><?= $label ?></button>
						</form>
					<?php
					}
					?>
				</div>
				<div class="col-md-6 col-lg-5 col-xl-4">
					<div class="card shadow-sm mb-3">
						<div class="card-header">
							<h2 class="h3 mb-0 text-center">Riepilogo</h2>
						</div>
						<ul class="list-group list-group-flush">
							<?php
							foreach ($checkout['products'] as $product)
							{
							?>
								<li class="list-group-item d-flex justify-content-between px-3">
									<a class="text-decoration-none h5 mb-0" href="product.php?id=<?= $product['id'] ?>"><?= $product['name'] ?></a>
									<p class="text-muted mb-0">&euro; <?= float_format($product['cost']) ?> &times; <?= $product['quantity'] ?> = &euro; <?= float_format($product['total']) ?></p>
								</li>
							<?php
							}
							if (is_value_not_null($checkout['save']))
							{
							?>
								<li class="list-group-item d-flex justify-content-between px-3">
									<h3 class="h5 text-success mb-0">Buono</h3>
									<p class="text-muted mb-0">- &euro; <?= float_format($checkout['save']) ?></p>
								</li>
							<?php
							}
							?>
							<li class="list-group-item d-flex justify-content-between px-3">
								<h3 class="h5 mb-0">Spedizione <?= $checkout['shipment'] ?></h3>
								<p class="text-muted mb-0">+ &euro; <?= float_format($checkout['shipment_cost']) ?></p>
							</li>
							<li class="list-group-item d-flex justify-content-between px-3">
								<h3 class="h5 mb-0">IVA (<?= $checkout['vat_percentage'] ?>%)</h3>
								<p class="text-muted mb-0">+ &euro; <?= float_format($checkout['vat']) ?></p>
							</li>
							<li class="list-group-item d-flex justify-content-between px-3">
								<h3 class="font-weight-bold h5 my-0">Totale</h3>
								<p class="font-weight-bold h5 my-0">&euro; <?= float_format($checkout['total']) ?></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>
