<?php
declare(strict_types = 1);
?>
<div class="row">
	<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
	</div>
	<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
		<div class="alert alert-<?= $message['type'] ?> alert-dismissable fade show h5" role="alert">
			<?= $message['text'] ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Chiudi">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	</div>
</div>
