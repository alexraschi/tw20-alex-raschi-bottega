<?php
declare(strict_types = 1);

if ($product['status'] === PRODUCT_NOT_FOUND)
{
	get_error_message('Prodotto non trovato.');
}
else
{
	if ($product['status'] === PRODUCT_INVALID_QUANTITY)
	{
		get_error_message('La quantità deve essere maggiore o uguale a 1.');
	}
	else if ($product['status'] === PRODUCT_ADDED)
	{
		get_success_message('Hai aggiunto questo prodotto al carrello!');
	}
	else if ($product['status'] === PRODUCT_UPDATED)
	{
		get_success_message('Hai modificato la quantità di questo prodotto nel carrello!');
	}
	else if ($product['status'] === PRODUCT_QUANTITY_UPDATED)
	{
		get_success_message('Hai aggiornato la quantità disponibile del prodotto!');
	}
	else if ($product['status'] === PRODUCT_DATABASE_ERROR)
	{
		get_success_message('Si è verificato un errore nell\'aggiornamento della quantità disponibile. Messaggio di errore: ' . $status['message']);
	}
?>
	<div class="text-center row">
		<div class="col-md-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h2 class="my-0 font-weight-normal"><?= $product['name'] ?></h2>
				</div>
				<div class="card-body">
					<a href="<?= $product['image'] ?>">
						<img class="img-fluid rounded" src="<?= $product['image'] ?>" alt="<?= $product['name'] ?>" />
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h2 class="h3 my-0 font-weight-normal">Informazioni</h2>
				</div>
				<div class="card-body text-left">
					<p>Ingredienti: <?= $product['ingredients'] ?></p>
					<p>Peso: <?= $product['weight'] ?> g</p>
					<p>Costo: &euro; <?= $product['cost'] ?></p>
					<p>Scadenza: <?= strtok($product['deadline'], ' ') ?></p>
					<h3 class="h4 font-weight-normal">Descrizione:</h3>
					<p class="mb-0"><?= $product['description'] ?></p>
				</div>
			</div>
			<?php
			if ($product['customer'])
			{
			?>
				<div class="card mb-3 shadow-sm">
					<div class="card-header">
						<h2 class="h3 my-0 font-weight-normal">Acquisto</h2>
					</div>
					<div class="card-body">
						<?php
						if ($product['quantity'] === 0)
						{
						?>
							<p class="text-danger h4 my-0">Questo prodotto non è disponibile.</p>
						<?php
						}
						else
						{
						?>
							<form method="post">
								<div class="mb-3">
								<?php
								get_input_form('quantity', true, 'Quantità', 'number', 'Quantità', 'min="' . $product['min'] . '" max="' . $product['max'] . '" required');
								if ($product['cart'])
								{
									$add = 'Aggiorna il carrello';
									$cart = 'Aggiorna e vai al carrello';
								}
								else
								{
									$add = 'Aggiungi al carrello';
									$cart = 'Aggiungi e vai al carrello';
								}
								?>
								</div>
								<button name="submit" value="add" type="submit" class="btn btn-lg btn-block btn-primary"><?= $add ?></button>
								<button name="submit" value="cart" type="submit" class="btn btn-lg btn-block btn-success"><?= $cart ?></button>
							</form>
						<?php
						}
						?>
					</div>
				</div>
			<?php
			}
			else if ($product['seller'])
			{
			?>
				<div class="card mb-3 shadow-sm">
					<div class="card-header">
						<h2 class="h3 my-0 font-weight-normal">Quantità disponibile</h2>
					</div>
					<div class="card-body">
						<form method="post">
							<div class="mb-3">
							<?php
							get_input_form('quantity', true, 'Quantità', 'number', to_string($product['quantity']), 'min="0" required');
							?>
							</div>
							<button name="submit" type="submit" class="btn btn-lg btn-block btn-success">Aggiorna quantità disponibile</button>
						</form>
					</div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
<?php
}
?>
