<?php
declare(strict_types = 1);

if ($login['status'] === LOGIN_ALREADY_DONE)
{
	get_error_message('Hai già effettuato l\'accesso.');
}
else
{
	if ($login['status'] === LOGIN_INPUT_ERROR)
	{
		get_error_message('Hai inserito le credenziali in modo errato.');
	}
	else if ($login['status'] === LOGIN_FROM_SIGNUP)
	{
		get_success_message('Hai effettuato correttamente la registrazione!');
	}
	else if ($login['status'] === LOGIN_FROM_LOGOUT)
	{
		get_success_message('Hai effettuato il logout con successo!');
	}

	if ($login['focus'] === 'email')
	{
		$login['email'] = ' autofocus';
		$login['password'] = '';
	}
	else
	{
		$login['email'] = '';
		$login['password'] = ' autofocus';
	}
?>
	<div class="row">
		<div class="col-sm-2 col-md-3 col-xl-4">
		</div>
		<div class="col-sm-8 col-md-6 col-xl-4">
			<div class="text-center">
				<form method="post">
					<div class="mb-2">
						<?php get_input_form('email', true, 'Indirizzo email', 'email', 'Indirizzo email', 'required' . $login['email']); ?>
					</div>
					<div class="mb-2">
						<?php get_input_form('password', true, 'Password', 'password', 'Password', 'required' . $login['password']); ?>
					</div>
					<div class="mb-2">
						<?php get_checkbox_form('customer', 'Login come cliente', 'checked', false, ''); ?>
					</div>
					<button class="btn btn-lg btn-primary btn-block mb-3" type="submit" name="submit">Accedi</button>
				</form>
			</div>
		</div>
	</div>
<?php
}
?>
