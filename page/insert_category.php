<?php
declare(strict_types = 1);

if ($insert_category['status'] === INSERT_CATEGORY_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come venditore prima di poter utilizzare questa pagina.');
}
else
{
	if ($insert_category['status'] === INSERT_CATEGORY_SUCCESS)
	{
		get_success_message('Hai inserito la nuova categoria con successo!');
	}
	else if ($insert_category['status'] === INSERT_CATEGORY_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la nuova categoria in modo parziale.');
	}
	else if ($insert_category['status'] === INSERT_CATEGORY_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni per la nuova categoria in modo errato. Messaggio di errore: ' . $insert_category['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
		</div>
		<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
			<form method="post">
				<div class="form-row mb-2">
					<?php get_input_form('name', true, 'Nome', 'text', 'Panini al salame', 'required'); ?>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 my-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiungi categoria</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
