<?php
declare(strict_types = 1);

if ($category['status'] === CATEGORY_NOT_FOUND)
{
	get_error_message('Categoria non trovata, torna indietro o selezionane un\'altra.');
}
else if ($category['status'] === CATEGORY_EMPTY)
{
	get_error_message('Categoria (temporaneamente) vuota, torna indietro o selezionane un\'altra.');
}
else
{
	if ($category['status'] === CATEGORY_FROM_LOGIN)
	{
		get_success_message('Benvenuto, hai effettuato correttamente l\'accesso!');
	}
	else if ($category['status'] === CATEGORY_FROM_CART_END)
	{
		get_success_message('Hai effettuato l\'ordine con successo!');
	}

	get_list($category['products'], $category['prefix'], $category['limit'], $category['offset'], $category['count']);
}
