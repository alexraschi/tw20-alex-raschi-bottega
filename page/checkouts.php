<?php
declare(strict_types = 1);

if ($checkouts['status'] === CHECKOUTS_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login prima di poter utilizzare questa pagina.');
}
else if ($checkouts['status'] === CHECKOUTS_EMPTY)
{
	get_error_message('Non sono presenti ordini al momento.');
}
else
{
?>
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-3 col-xl-4">
		</div>
		<div class="col-sm-10 col-md-8 col-lg-6 col-xl-4">
			<ul class="shadow-sm list-group mb-3">
				<?php
				foreach ($checkouts['list'] as $checkout)
				{
					if ($checkout['issent'])
					{
						$text = 'success';
						$state = 'Inviato';
					}
					else if ($checkout['isworking'])
					{
						$text = 'primary';
						$state = 'In lavorazione';
					}
					else if ($checkout['isaccepted'])
					{
						$text = 'secondary';
						$state = 'Accettato';
					}
				?>
					<li class="list-group-item">
						<a class="text-decoration-none text-<?= $text ?>" href="checkout.php?id=<?= $checkout['id'] ?>">Ordine: <?= $checkout['id'] ?>, Stato: <?= $state ?>.</a>
					</li>
				<?php
				}
				?>
			</ul>
		</div>
	</div>
<?php
}
?>
