<?php
declare(strict_types = 1);

if ($cart['status'] === CART_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come cliente prima di poter utilizzare questa pagina.');
}
else
{
	if ($cart['status'] === CART_FROM_ADDED_PRODUCT)
	{
		get_success_message('Hai aggiunto il prodotto al carrello!');
	}
	else if ($cart['status'] === CART_FROM_UPDATED_PRODUCT || $cart['status'] === CART_PRODUCT_UPDATED)
	{
		get_success_message('Hai modificato la quantità del prodotto nel carrello!');
	}
	else if ($cart['status'] === CART_DISCOUNT_ERROR)
	{
		get_error_message('Il codice che hai inserito non è valido oppure è già stato usato.');
	}
	else if ($cart['status'] === CART_DISCOUNT_ADDED)
	{
		get_success_message('Hai inserito correttamente il codice per un buono valido!');
	}
	else if ($cart['status'] === CART_PRODUCT_REMOVED)
	{
		get_success_message('Hai rimosso il prodotto dal carrello!');
	}
	else if ($cart['status'] === CART_CLEARED)
	{
		get_success_message('Hai rimosso tutto dal carrello!');
	}
	else if ($cart['status'] === CART_EMPTY)
	{
		get_error_message('Il carrello è vuoto! Aggiungi dei prodotti cercandoli oppure utilizzando le categorie.');
	}
?>
	<div class="row">
		<div class="col-md-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-md-10 col-lg-8 col-xl-6">
			<div class="mb-3">
				<h2 class="h3 d-flex justify-content-between align-items-center mb-3">
					<span class="text-muted">Prodotti</span>
					<span class="badge badge-pill badge-secondary"><?= $cart['count'] ?></span>
				</h2>
				<ul class="list-group">
					<?php
					$max = 40;
					foreach ($cart['products'] as $id => $product)
					{
						if (mb_strlen($product['description']) > $max)
						{
							$product['description'] = substr($product['description'], 0, $max) . '...';
						}
					?>
						<li class="list-group-item px-3">
							<div class="row">
								<div class="col-12 col-sm-7">
									<a class="text-decoration-none d-flex" href="product.php?id=<?= $id ?>">
										<img class="img-fluid rounded mr-3" src="<?= $product['image'] ?>" alt="<?= $product['name'] ?>" />
										<div>
											<h3 class="h5 mb-1"><?= $product['name'] ?></h3>
											<p class="small text-muted mb-0"><?= $product['description'] ?></p>
										</div>
									</a>
								</div>
								<div class="col-12 col-sm-5 d-flex justify-content-end">
									<form method="post" class="form-inline mt-2 mt-sm-0">
										<p class="text-muted mb-0">&euro; <?= float_format($product['cost']) ?> &times; <?php get_select_form($product['selectid'], true, 'Quantità', 'auto-submit', false, $product['select'], ''); ?> = &euro; <?= float_format($product['total']) ?></p>
									</form>
								</div>
							</div>
						</li>
					<?php
					}
					if (is_value_not_null($cart['discount']))
					{
					?>
						<li class="list-group-item d-flex justify-content-between px-3">
							<div class="text-success">
								<h3 class="h5 mb-1">Buono</h3>
								<p class="small text-muted mb-0"><?= $cart['discount']['code'] ?></p>
							</div>
							<p class="text-muted">- &euro; <?= float_format($cart['discount']['save']) ?></p>
						</li>
					<?php
					}
					?>
					<li class="list-group-item d-flex justify-content-between px-3">
						<h3 class="h5 mb-0">Spedizione <?= $cart['shipment']['name'] ?></h3>
						<p class="text-muted mb-0">+ &euro; <?= float_format($cart['shipment']['cost']) ?></p>
					</li>
					<li class="list-group-item d-flex justify-content-between px-3">
						<h3 class="h5 mb-0">IVA (<?= $cart['vat_percentage'] ?>%)</h3>
						<p class="text-muted mb-0">+ &euro; <?= float_format($cart['vat']) ?></p>
					</li>
					<li class="list-group-item d-flex justify-content-between px-3">
						<h3 class="font-weight-bold h5 my-0">Totale</h3>
						<p class="font-weight-bold h5 my-0">&euro; <?= float_format($cart['total']) ?></p>
					</li>
				</ul>
			</div>
			<div class="input-group mb-3">
				<form method="post" class="input-group">
					<input type="text" class="form-control" id="discount" name="discount" placeholder="Codice buono" minlength="<?= $cart['discount_length'] ?>" maxlength="<?= $cart['discount_length'] ?>" />
					<label for="discount" class="sr-only">Codice buono</label>
					<div class="input-group-append">
						<button type="submit" class="btn btn-secondary">Includi buono</button>
					</div>
				</form>
			</div>
			<?php
			if (!$cart['expensive'])
			{
			?>
				<div class="mb-3">
					<form method="post">
						<h2 class="h3">Spedizione</h2>
						<div class="mb-3">
							<?php
							foreach ($cart['shipments'] as $id => $shipment)
							{
								$labels[$id] = $shipment['name'] . ' &euro; ' . $shipment['cost'];
							}
							get_radio_form('shipment', 'Spedizione', $labels, $cart['shipment']['id'], 'auto-submit', 'required');
							?>
						</div>
					</form>
				</div>
			<?php
			}
			?>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<button type="button" class="btn btn-lg btn-block btn-danger" data-toggle="modal" data-target="#modal-clear">Rimuovi tutto</button>
				</div>
				<div class="col-md-6 mb-3">
					<a href="cart_end.php" class="btn btn-primary btn-lg btn-block">Continua con l'ordine</a>
				</div>
			</div>
			<div class="modal fade" id="modal-clear" tabindex="-1" aria-labelledby="modal-clear-title" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h1 class="h5 modal-title" id="modal-clear-title">Rimuovi tutto dal carrello</h1>
							<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>Vuoi veramente rimuovere tutto dal carrello?</p>
						</div>
						<div class="modal-footer">
							<form method="post">
								<button name="clear" type="submit" class="btn btn-block btn-danger modal-focus">Rimuovi tutto</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>
