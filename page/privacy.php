<?php
declare(strict_types = 1);
?>
<div class="row">
	<div class="col-sm-1 col-md-2 col-lg-3">
	</div>
	<div class="col-sm-10 col-md-8 col-lg-6 mb-3">
		<p>In questa pagina sono descritte le modalità di gestione del sito web in riferimento al trattamento dei dati personali dei visitatori e dei clienti che lo consultano, nonché le pratiche di trattamento dei dati trasmessi dall'interessato al Titolare tramite questo sito. Si tratta di un'informativa resa ai sensi degli artt. 13 e 14 Regolamento UE 2016/679 (da ora Regolamento) a coloro che interagiscono con i servizi web della società Gianpiero Productions Srl con sede in Via Marecchiese 1657 Rimini (Italia). L'informativa è resa per il sito in questione e non anche per altri siti web eventualmente consultati dall'utente tramite link.</p>
		<h2 class="my-3">Titolare del trattamento</h2>
		<p>Il titolare del trattamento dei dati raccolti sul sito a seguito della consultazione, dell'acquisto o della volontaria registrazione in esso, è Gianpiero Productions Srl con sede in Via Marecchiese 1657 Rimini (Italia), Codice Fiscale e Partita IVA 36746569322, email <a href="mailto:gianpiero-privacy@gmail.com">gianpiero-privacy@gmail.com</a>.</p>
		<h2 class="my-3">I principi di riferimento</h2>
		<p>I trattamenti effettuati da Gianpiero Productions Srl sono improntati ai principi di trasparenza, pertinenza, finalità, verificabilità, liceità, correttezza, limitazione delle finalità e della conservazione, minimizzazione dei dati, esattezza, integrità e riservatezza.</p>
		<h2 class="my-3">Luogo del trattamento dei dati</h2>
		<p>I trattamenti connessi ai servizi di questo sito hanno luogo presso la sede legale della Gianpiero Productions Srl sita in Via Marecchiese 1657 Rimini (Italia), e sono curati solo dal personale del Titolare del trattamento.</p>
		<h2 class="my-3">Soggetti autorizzati al trattamento, responsabili e comunicazione dei dati</h2>
		<p>Il trattamento dei dati raccolti è effettuato da personale interno di Gianpiero Productions Srl nel rispetto della normativa vigente.</p>
		<p>Per l'erogazione dei servizi i tuoi dati potrebbero essere trattati da nostri selezionati fornitori che agiscono in qualità di responsabili del trattamento (art. 28 del Regolamento). Per esempio:</p>
		<ol>
			<li>professionisti o aziende di assistenza e consulenza contabile, amministrativa, legale, tributaria, finanziaria, tecnica</li>
			<li>professionisti o aziende necessari per l'erogazione del servizio (es. corrieri, hosting provider)</li>
			<li>soggetti, enti od autorità, per finalità amministrative e per l'adempimento di obblighi di legge, quali quelli di natura contabile, fiscale, o per dar seguito a richieste dell'autorità giudiziaria.</li>
		</ol>
		<p>In ogni caso, i dati personali non saranno mai diffusi.</p>
		<h2 class="my-3">Dati trattati, finalità e basi giuridiche</h2>
		<h3 class="my-3">Dati generati dall'accesso al sito</h3>
		<p>I sistemi informatici e le procedure software preposte al funzionamento di questo sito web acquisiscono, nel corso del loro normale esercizio, alcuni dati personali la cui trasmissione è implicita nell'uso dei protocolli di comunicazione di Internet. Si tratta di informazioni che non sono raccolte per essere associate a interessati identificati, ma che per loro stessa natura potrebbero, attraverso elaborazioni e associazioni con dati detenuti da terzi, permettere di identificare gli utenti. In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli utenti che si connettono al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) delle risorse richieste, l'orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) e altri parametri relativi al sistema operativo e all'ambiente informatico dell'utente. Questi dati vengono utilizzati al solo fine di ricavare informazioni statistiche anonime sull'uso del sito e per controllarne il corretto funzionamento e vengono cancellati immediatamente dopo l'elaborazione.</p>
		<h3 class="my-3">Dati forniti volontariamente dall'utente</h3>
		<p>Gianpiero Productions Srl tratterà i dati personali dell'utente per:</p>
		<ul>
			<li>consentire la navigazione, la registrazione di un account, l'acquisto di beni e servizi;</li>
			<li>memorizzare i dati anagrafici, lo storico degli ordini, gli indirizzi di fatturazione e consegna;</li>
			<li>consentire la conclusione del contratto di acquisto e la corretta esecuzione delle obbligazioni nascenti da tale contratto (es. la consegna);</li>
		</ul>
		<p>La base giuridica per questo trattamento è l'adempimento del contratto. Per questa finalità, Gianpiero Productions Srl tratterà i dati dell'utente per il tempo strettamente necessario allo svolgimento delle singole attività di trattamento. Il conferimento dei dati per la finalità citate è facoltativo; tuttavia, poiché il loro conferimento è necessario per consentire l'erogazione dei servizi, il mancato conferimento degli stessi comporterà l'impossibilità di concludere un contratto di acquisto e/o di ricevere risposta alle richieste effettuate.</p>
		<p>Se fornisci dati personali di terzi, come ad esempio quelli dei tuoi familiari o amici, devi essere sicuro che questi soggetti siano stati adeguatamente informati e abbiano acconsentito al relativo trattamento nelle modalità descritte dalla presente informativa. Ti informiamo che Gianpiero Productions Srl non tratta dati personali relativi ai minori d'età. Se accedi a questo sito e utilizzi i servizi offerti da Gianpiero Productions Srl dichiari di avere compiuto la maggiore età.</p>
		<p>Gianpiero Productions Srl non registra o conserva informazioni delle carte di credito.</p>
		<h2 class="my-3">Cookies</h2>
		<p>Nessun dato personale degli utenti viene in proposito acquisito dal sito tramite cookies. Non viene fatto uso di cookies per la trasmissione di informazioni di carattere personale.</p>
		<h2 class="my-3">Modalità di trattamento e tempi di conservazione dei dati</h2>
		<p>I dati personali sono trattati con strumenti automatizzati per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti, nel rispetto del principio di minimizzazione del trattamento dei dati. Vengono osservate specifiche misure di sicurezza al fine di evitare la perdita dei dati, usi illeciti o non corretti e accessi non autorizzati.</p>
		<h2 class="my-3">Diritti degli interessati</h2>
		<p>Ai sensi degli articoli 15, 16, 17, 18, 19, 20 e 21 del Regolamento, l'interessato al trattamento dei dati ha diritto ha il diritto di</p>
		<ul>
			<li>ottenere la conferma che sia o meno in corso un trattamento di dati personali e, in tal caso, di ottenere l'accesso ai dati personali;</li>
			<li>ottenere la rettifica dei dati inesatti;</li>
			<li>ottenere la cancellazione dei dati pesonali (oblio);</li>
			<li>ottenere la limitazione del trattamento;</li>
			<li>ricevere in un formato strutturato, di uso comune, i dati personali che lo riguardano e trasmettere tali dati a un altro titolare (portabilità)</li>
		</ul>
		<p>In ogni momento, l'utente può revocare il consenso prestato. Le richieste vanno rivolte per iscritto al seguente indirizzo: <a href="mailto:gianpiero-assistenza@gmail.com">gianpiero-assistenza@gmail.com</a>.</p>
		<p>La presente informativa potrà subire modifiche e integrazioni nel corso del tempo, ti invitiamo, pertanto, a verificarne periodicamente i contenuti.</p>
	</div>
</div>
