<?php
declare(strict_types = 1);

if ($search['status'] === SEARCH_EMPTY)
{
	get_error_message('La ricerca non ha prodotto risultati, riprova con altri termini.');
}
else
{
	get_list($search['products'], $search['prefix'], $search['limit'], $search['offset'], $search['count']);
}
