<?php
declare(strict_types = 1);

if ($update_password['status'] === UPDATE_PASSWORD_LOGIN_ERROR)
{
	get_error_message('Devi effettuare il login come cliente prima di poter utilizzare questa pagina.');
}
else
{
	if ($update_password['status'] === UPDATE_PASSWORD_SUCCESS)
	{
		get_success_message('Hai modificato la tua password con successo!');
	}
	else if ($update_password['status'] === UPDATE_PASSWORD_INPUT_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo parziale.');
	}
	else if ($update_password['status'] === UPDATE_PASSWORD_CHECK_ERROR)
	{
		get_error_message('La due password che hai immesso non coincidono.');
	}
	else if ($update_password['status'] === UPDATE_PASSWORD_DATABASE_ERROR)
	{
		get_error_message('Hai inserito le informazioni in modo errato. Messaggio di errore: ' . $update_password['message']);
	}
?>
	<div class="row">
		<div class="col-sm-1 col-lg-2 col-xl-3">
		</div>
		<div class="col-sm-10 col-lg-8 col-xl-6">
			<form method="post">
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_input_form('old-password', false, 'Vecchia Password', 'password', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('password', false, 'Nuova Password', 'password', '', 'required'); ?>
					</div>
					<div class="col-md-4 mb-3">
						<?php get_input_form('password-check', false, 'Ripeti password', 'password', '', 'required'); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<?php get_checkbox_form('show-password', 'Mostra password', '', true, ''); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-2 col-lg-3">
					</div>
					<div class="col-md-8 col-lg-6 mb-3">
						<button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Aggiorna password</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
}
?>
