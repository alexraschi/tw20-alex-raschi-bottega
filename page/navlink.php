<?php
declare(strict_types = 1);
?>
<li class="nav-item<?php if ($navlink['current']) { echo ' active'; } ?>">
	<a class="nav-link text-nowrap" href="<?= $navlink['target'] ?>"><?php echo $navlink['name']; if ($navlink['current']) { echo ' <span class="sr-only">(Pagina corrente)</span>'; } ?></a>
</li>
