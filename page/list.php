<?php
declare(strict_types = 1);
?>
<ol class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5 list-unstyled">
	<?php
	foreach($list['products'] as $product)
	{
	?>
		<li class="col">
			<a class="card mb-3 shadow-sm text-decoration-none" href="product.php?id=<?= $product['id'] ?>">
				<div class="card-header">
					<h2 class="h4 my-0 font-weight-normal text-center"><?= $product['name'] ?> &euro; <?= $product['cost'] ?></h2>
				</div>
				<div class="card-body">
					<p class="text-dark"><?= $product['description'] ?></p>
					<img class="img-fluid rounded" src="<?= get_image_path($product['image']) ?>" alt="<?= $product['name'] ?>" />
				</div>
			</a>
		</li>
	<?php
	}
	?>
</ol>
<div class="mb-3 row">
	<div class="col-md-3">
	</div>
	<div class="col-6 col-md-3">
		<?php
		if ($list['offset'] > 0)
		{
		?>
			<a href="<?= $list['prefix'] ?>&limit=<?= $list['limit'] ?>&offset=<?= $list['offset'] - $list['limit'] ?>" class="btn btn-lg btn-block btn-primary">Indietro</a>
		<?php
		}
		?>
	</div>
	<div class="col-6 col-md-3">
		<?php
		if ($list['limit'] + $list['offset'] < $list['count'])
		{
		?>
			<a href="<?= $list['prefix'] ?>&limit=<?= $list['limit'] ?>&offset=<?= $list['offset'] + $list['limit'] ?>" class="btn btn-lg btn-block btn-primary">Avanti</a>
		<?php
		}
		?>
	</div>
</div>
